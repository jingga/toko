$(document).ready(function(){
    var curr_menu = $('#curr_menu').attr('data-value');

    var get_list_url = $('#url_all_list').attr('data-url');

    $('#income_outcome_table').DataTable({
        "deferRender"    : true,
        "scrollX"        : true,
        "scrollCollapse" : true,
        "scroller"       : true,
        "ajax"          : {
            "url" : get_list_url,
            "type": "POST",
            "headers" : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            }
        },
        "columns"       : [
                {"data"   : 'tanggal'},
                {"data"   : 'keterangan', sortable   : false},
                {"data"   : 'jumlah', searchable  : false, sortable   : false}
        ],
        "columnDefs"    : [
            {
                targets : [0,1],
                "class" : "mdl-data-table__cell--non-numeric"
            }
        ],
        "order"         : [[ 0, 'desc' ],[1,'asc']]
    });

    $('input[name="tanggal"]').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight: true,
        autoclose: true,
        endDate : 'now'
    });

    $('input[name="jumlah"]').priceFormat({
        prefix: 'Rp ',
        centsLimit: 0,
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    $('#add_new_income_outcome').on('click',function(){
        $('#modal_income_outcome').modal();
    });

    $('#modal_submit').on('click', function()
    {
        var url = $('#url_save_income_outcome').attr('data-url');

        $('#modal_income_outcome input.form-control').each(function(){
            var current_data = $(this).val();

            if(!current_data)
            {
                $(this).addClass('invalid');
                $(this).attr('title', 'Tidak Boleh Kosong');
                $(this).tooltip();
            }
            else
            {
                $(this).removeClass('invalid');
                $(this).removeAttr('title');
                $(this).tooltip('destroy');
            }
        });

        if($('.invalid').length > 0)
            return false;

        $('#modal_income_outcome').modal('hide');

        before_submit_change_to_int($('input[name="jumlah"]'));

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: url,
            data: $('#submit_income_outcome').serialize(),
            success: function(data) {
                // general_function
                get_alert_status_object(data);
                $('#submit_income_outcome')[0].reset();
                $('#income_outcome_table').DataTable().ajax.reload();
            },
            error: function(e) {
                //general_function
                get_alert_error();
            }
        });
    })
});