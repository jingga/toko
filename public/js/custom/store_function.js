$(document).ready(function(){
    var dataTable = $('#store_table').DataTable({
        "deferRender"    : true,
        "scrollX"        : true,
        "scrollCollapse" : true,
        "scroller"       : true,
        "ajax"          : {
            "url" : $('#url_all_store').attr('data-url'),
            "type": "POST",
            "headers" : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            }
        },
        "columns"       : [
                {"data"   : '', searchable  : false, render:function(data,type,row,meta){
                    return '<span>' + (meta.row +1) + '</span>';
                }},
                {"data"   : 'nama_toko'},
                {"data"   : 'url_toko'},
                {"data"   : 'logo', searchable  : false, sortable   : false, render:function(data,type,row,meta){
                    if(!data)
                    {
                        data = '/images/no-thumbnail.png';
                    }
                    var html = '<img src="' + data +  '" class="img-thumbnail" />';
                    return html;
                }},
                {"data"   : 'status', searchable  : false, render:function(data,type,row,meta){
                    var html = '';
                    if(data == 'ACTIVE')
                        html = '<p class="label label-success">' + data + '</p>';
                    else   
                        html = '<p class="label label-danger">' + data + '</p>';
                    return html;
                }},
                {"data"   : 'id', "class" : "text-center" ,searchable  : false, sortable   : false, render:function(data,type,row,meta){
                    var html ='<button class="btn btn-sm btn-info edit_store" data-value="' + data + '"><i class="glyphicon glyphicon-pencil"></i> Edit</button>' +
                                '&nbsp;&nbsp;';

                    html += '&nbsp;&nbsp;<button class="btn btn-sm btn-danger delete_store" data-value="' + data + '"><i class="glyphicon glyphicon-trash"></i> Delete</button>' ;
                    return html;
                }}
        ],
        "columnDefs"    : [
            {
                targets : [1,2,3,4],
                "class" : "mdl-data-table__cell--non-numeric"
            }
        ],
        "order"         : [[ 0, 'asc' ],[1,'asc']]
    });

    $('#add_new_user').on('click',function(){
        $('#modal_title').text('Toko Baru');
        $('#modal_submit').attr('data-target','add');

        //empty all value
        $('#submit_store')[0].reset();
        $('input[name="id_store"]').val('');
        removeUpload();
        $('#modal_store').modal();
    });

    $('#store_table').on('click', '.edit_store', function(){
        var selected_row = $(this).closest('tr').find('td');
        var curr_id = $(this).attr('data-value');
        
        $('#modal_title').text('Edit Toko');
        $('#modal_submit').attr('data-target','edit');

        //populate the modal
        var index = 0;
        $('#modal_store *').filter('.form-control').each(function(){
            var current_tag = $(this);
            var current_val = selected_row.eq(index);

            if(index == 0)
                current_val = curr_id;
            else if(current_tag.hasClass('file-upload-input'))
            {
                current_val = current_val.find('img').attr('src');
            }
            else
                current_val = current_val.text();
                
            if(current_tag.hasClass('file-upload-input'))
            {
                $('.image-upload-wrap').hide();

                $('.file-upload-image').attr('src', current_val);
                $('.file-upload-content').show();
            }
            else
                current_tag.val(current_val);
            
            index++;
        });

        $('#modal_store').modal();
    });

    $('#store_table').on('click', '.delete_store', function(){
        //empty all value
        $('#submit_store')[0].reset();
        $('input[name="id_store"]').val('');

        var selected_row = $(this).closest('tr').find('td');

        //get the id to input
        var selected_id = $(this).attr('data-value');
        $('input[name="id_store"]').val(selected_id);

        var store_name = selected_row.eq(1).text();

        $('#warning_title').text('Hapus Toko');
        $('#warning_content').text('Apakah Anda yakin ingin menghapus ' + store_name + '?');
        $('#modal_warning').modal();
    });

    // delete
    $('#submit_warning').on('click', function(){
        $('#modal_warning').modal('hide');

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: $('#delete_store').attr('data-url'),
            data: {'id_store': $('input[name="id_store"]').val()},
            success: function(data) {
                // general_function
                get_alert_status_object(data);

                $('#store_table').DataTable().ajax.reload();
            },
            error: function(e) {
                //general_function
                get_alert_error();
            }
        });
    });

    $('#submit_store').on('click', '.file-upload-btn', function()
    {
        $('.file-upload-input').click();
    });

    $('#submit_store').on('change', '.file-upload-input', function(e)
    {
        readURL(e.target);
    });

    $('#submit_store').on('click', '.remove-image', function()
    {
        removeUpload();
    });

    function readURL(input)
    {
        if (input.files && input.files[0])
        {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.image-upload-wrap').hide();

                $('.file-upload-image').attr('src', e.target.result);
                $('.file-upload-content').show();
            };

            reader.readAsDataURL(input.files[0]);

        }
        else
        {
            removeUpload();
        }
    }

    function removeUpload()
    {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }

    $('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });

    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
    });

    // add edit
    $('#modal_submit').on('click', function(){
        var submit = $('#modal_submit').attr('data-target');
        $('#modal_store').modal('hide');

        var form_data = new FormData($('#submit_store')[0]);

        var url = '';

        if(submit == 'add')
                url =$('#insert_store').attr('data-url');
        else if(submit == 'edit')
            url =$('#update_store').attr('data-url');
        
        if(url)
        {
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                url: url,
                data: form_data,
                processData: false,
                contentType: false,
                success: function(data) {
                    // general_function
                    get_alert_status_object(data);

                    $('#store_table').DataTable().ajax.reload();
                },
                error: function(e) {
                    //general_function
                    get_alert_error();
                }
            });
        }
        else
        {
            return false;
        }
    });
});