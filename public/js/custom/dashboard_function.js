$(document).ready(function(){
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        url: $('#url_load_dashboard').attr('data-url'),
        success: function(data) {
            if(data)
            {
                // this month
                $('#today_income').text(data.today.income);
                $('#today_outcome').text(data.today.outcome);
                $('#today_profit').text(data.today.profit);
                $('#today_turnover').text(data.today.turnover);

                // this month
                $('#month_income').text(data.this_month.income);
                $('#month_outcome').text(data.this_month.outcome);
                $('#month_profit').text(data.this_month.profit);
                $('#month_turnover').text(data.this_month.turnover);

                // this year
                $('#year_income').text(data.this_year.income);
                $('#year_outcome').text(data.this_year.outcome);
                $('#year_profit').text(data.this_year.profit);
                $('#year_turnover').text(data.this_year.turnover);
            }
        },
        error: function(e) {
            // general_function
            get_alert_error();
        }
    });

    $('#summary').on('click','a[data-toggle="collapse"]',function(){
        var glyph_tag = $(this).find('i');
        var all_glyph = $('a[data-parent="#summary"] > i.glyphicon-chevron-up');
        
        if(glyph_tag.hasClass('glyphicon-chevron-up'))
        {
            for(var i = 0; i<all_glyph.length; i++)
            {
                all_glyph.eq(i).removeClass('glyphicon-chevron-up');
                all_glyph.eq(i).addClass('glyphicon-chevron-down');
            }
            glyph_tag.removeClass('glyphicon-chevron-up');
            glyph_tag.addClass('glyphicon-chevron-down');
        }
        else if(glyph_tag.hasClass('glyphicon-chevron-down'))
        {
            for(var i = 0; i<all_glyph.length; i++)
            {
                all_glyph.eq(i).removeClass('glyphicon-chevron-up');
                all_glyph.eq(i).addClass('glyphicon-chevron-down');
            }
            glyph_tag.removeClass('glyphicon-chevron-down');
            glyph_tag.addClass('glyphicon-chevron-up');
        }
    });
});