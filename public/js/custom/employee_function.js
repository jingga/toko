$(document).ready(function(){
    $('input[name="salary"], input[name="bonus_monthly"], input[name="bonus"]').priceFormat({
        prefix: 'Rp ',
        centsLimit: 0,
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    
    var employeeTable = $('#employee_table').DataTable({
        "deferRender"    : true,
        "scrollX"        : true,
        "scrollCollapse" : true,
        "scroller"       : true,
        "progress"       : true,
        "ajax"          : {
            "url" : $('#url_all_employee').attr('data-url'),
            "type": "POST",
            "headers" : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            }
        },
        "columns"       : [
                {"data"   : 'nama'},
                {"data"   : 'salary', searchable  : false},
                {"data"   : 'jenis_gaji', searchable  : false, sortable : false},
                {"data"   : 'waktu_bayar', searchable  : false, sortable : false},
                {"data"   : 'bulanan', searchable  : false, sortable : false},
                {"data"   : 'bonus', searchable  : false, sortable : false},
                {"data"   : 'status', searchable  : false, sortable : false, render:function(data,type,row,meta){
                    var html = '';
                    if(data == 'ACTIVE')
                        html = '<p class="label label-success">' + data + '</p>';
                    else   
                        html = '<p class="label label-danger">' + data + '</p>';
                    return html;
                }},
                {"data"   : 'id', "class" : "text-center" ,searchable  : false, sortable   : false, render:function(data,type,row,meta){
                    var html ='<button class="btn btn-sm btn-info edit_employee" data-value="' + data + '"><i class="glyphicon glyphicon-pencil"></i> Edit</button>' +
                                '&nbsp;&nbsp;' +
                                '<button class="btn btn-sm btn-danger delete_employee" data-value="' + data + '"><i class="glyphicon glyphicon-trash"></i> Delete</button>' ;
                    return html;
                }}
        ],
        "columnDefs"    : [
            {
                targets : [0,2,3,6,7],
                "class" : "mdl-data-table__cell--non-numeric"
            }
        ],
        "order"         : [[ 0, 'asc' ]]
    });

    $('#absence_table').DataTable({
        "deferRender"    : true,
        "scrollX"        : true,
        "scrollCollapse" : true,
        "scroller"       : true,
        "progress"       : true,
        "ajax"          : {
            "url" : $('#url_employee_absence').attr('data-url'),
            "type": "POST",
            "headers" : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            }
        },
        "columns"       : [
                {"data"   : 'nama'},
                {"data"   : 'status', searchable  : false, sortable : false, render:function(data,type,row,meta){
                    var checkbox = false;

                    if(data == 'MASUK')
                        checkbox = true;

                    var html = '<div class="form-check">' +
                            '<input class="form-check-input absence" type="checkbox" id="' + (meta.row +1) + '" data-value="' +
                            row.id +
                            '" ';
                        
                    if(checkbox)
                        html += 'checked';
                        
                    html += ' />' +
                        '<label class="form-check-label" for="' + (meta.row +1) + '">Masuk</label>' +
                    '</div>';
                        
                    return html;
                }},
                {"data"   : 'salary', searchable  : false},
        ],
        "columnDefs"    : [
            {
                targets : [0,1],
                "class" : "mdl-data-table__cell--non-numeric"
            }
        ],
        "order"         : [[ 0, 'asc' ]]
    });

    $('#add_new_employee').on('click',function(){
        $('#modal_title').text('Karyawan Baru');
        $('#modal_submit').attr('data-target','add');

        //empty all value
        $('#submit_employee')[0].reset();
        $('input[name="id_karyawan"]').val('');
        $('#modal_employee').modal();
    });

    $('#employee_table').on('click', '.edit_employee', function(){
        //empty all value
        $('input[name="id_karyawan"]').val('');
        $('#submit_employee')[0].reset();

        var selected_row = $(this).closest('tr').find('td');
        var curr_id = $(this).attr('data-value');
        
        $('#modal_title').text('Edit Karyawan');
        $('#modal_submit').attr('data-target','edit');

        //populate the modal
        var index = 0;
        $('#modal_employee *').filter('.form-control').each(function(){
            var current_tag = $(this);
            var current_val = selected_row.eq(index);

            if(current_tag.attr('name') == 'id_karyawan')
                current_val = curr_id;
            else
                current_val = current_val.text();
                
            current_tag.val(current_val);
            index++;
        });

        $('#modal_employee').modal();
    });


    $('#employee_table').on('click', '.delete_employee', function(){
        //empty all value
        $('input[name="id_karyawan"]').val('');
        $('#submit_employee')[0].reset();

        var selected_row = $(this).closest('tr').find('td');

        //get the id to input
        var selected_id = $(this).attr('data-value');
        $('input[name="id_karyawan"]').val(selected_id);

        var employee_name = selected_row.eq(0).text();

        $('#warning_title').text('Hapus Karyawan');
        $('#warning_content').text('Apakah Anda yakin ingin menghapus ' + employee_name + '?');
        $('#modal_warning').modal();
    });

    // delete
    $('#submit_warning').on('click', function(){
        $('#modal_warning').modal('hide');

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: $('#url_delete_employee').attr('data-url'),
            data: {'id_karyawan': $('input[name="id_karyawan"]').val()},
            success: function(data) {
                // general_function
                get_alert_status_object(data);

                $('#employee_table').DataTable().ajax.reload();
                $('#absence_table').DataTable().ajax.reload();
            },
            error: function(e) {
                //general_function
                get_alert_error();
            }
        });
    });


    $('#modal_submit').on('click', function(){
        var submit = $('#modal_submit').attr('data-target');
        var url = '';
        $('#modal_employee').modal('hide');
        
        if(submit == 'add')
        {
            // currency salary
            before_submit_change_to_int($('input[name="salary"]'));

            // currency bonus monthly
            before_submit_change_to_int($('input[name="bonus_monthly"]'));

            // currency bonus
            before_submit_change_to_int($('input[name="bonus"]'));

            url = $('#url_insert_employee').attr('data-url');
        }
        else if (submit == 'edit')
        {
            // currency salary
            before_submit_change_to_int($('input[name="salary"]'));

            // currency bonus monthly
            before_submit_change_to_int($('input[name="bonus_monthly"]'));

            // currency bonus
            before_submit_change_to_int($('input[name="bonus"]'));
            
            url = $('#url_update_employee').attr('data-url');
        }
        else
        {
            return false;
        }

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: url,
            data: $('#submit_employee').serialize(),
            success: function(data) {
                // general_function
                get_alert_status_object(data);

                $('#employee_table').DataTable().ajax.reload();
                $('#absence_table').DataTable().ajax.reload();
            },
            error: function(e) {
                //general_function
                get_alert_error();
            }
        });
    });

    $('#absence_table').on('click','.absence', function(){
        var curr_checkbox = $(this);
        var curr_id = curr_checkbox.attr('data-value');
        var data_request = {};

        if(curr_checkbox.is(":checked"))
        {
            data_request = {id : curr_id, status : 'MASUK'};
        }
        else
        {
            data_request = {id : curr_id, status : 'ABSEN'}
        }
        
        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: $('#url_update_absence').attr('data-url'),
            data: data_request,
            success: function(data) {
            },
            error: function(e) {
            }
        });
    });

});