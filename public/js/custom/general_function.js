var loading = $('#loader');
$(document).ajaxStart(function () {
    loading.removeClass('hide');
})
.ajaxComplete(function (event, xhr, settings) {
    if (settings.url != undefined && settings.url === $('#url_update_absence').attr('data-url')) {
        $('#absence_table').DataTable().ajax.reload();
    }
    loading.addClass('hide');
});

$(document).ready(function(){
    var curr_menu = $('#curr_menu').attr('data-value').toLowerCase();

    if(curr_menu !== 'index')
    {
        var element = $('a.' + curr_menu);
        element.addClass('active-menu');
    }
    
});

function change_int_to_currency (ammount)
{
    var minus = '';
    if(ammount < 0)
        minus = '-';

    var reverse = ammount.toString().split('').reverse().join(''),
        ribuan  = reverse.match(/\d{1,3}/g);
    var convert_to_currency  = ribuan.join('.').split('').reverse().join('');

    return 'Rp ' + minus + convert_to_currency;
}

function before_submit_change_to_int(element)
{
    var parse_to_int = 0;
    
    if(element.val() || element.val().length > 0)
        parse_to_int = parseInt(element.unmask().slice(1));

    element.val(parse_to_int);
}

function get_alert_status_object(data)
{
    var html_alert = '<div class="alert ';
    var message = '';

    if(typeof data =='object')
    {
        if(data.code == 200)
        {
            html_alert += 'alert-success';
        }
        else
        {
            html_alert += 'alert-danger';
        }
        
        message = data.message;
    }
    else
    {
        html_alert += 'alert-danger';
        message = 'Terjadi Kesalahan pada server';
    }

    html_alert += '">' + message + '</div>';
    $('#alert').html(html_alert);

    $('#alert').removeClass('hide');
    setTimeout(function() {
        $("#alert").addClass('hide');
        $('#alert').empty();
    }, 3000);
}
function get_alert_error()
{
    $('#alert').removeClass('alert-danger');
    $('#alert').removeClass('alert-warning');
    $('#alert').removeClass('alert-success');

    $('#alert').addClass('alert-danger');
    $('#alert').text('Terjadi Kesalahan pada server');

    $('#alert').removeClass('hide');
    setTimeout(function() { $("#alert").addClass('hide'); }, 3000);
}

function typeNumber(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}