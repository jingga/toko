var curr_click = '';

$('input[name="tanggal"]').datepicker({
    format: 'dd-mm-yyyy',
    todayHighlight: true,
    autoclose: true,
    endDate : 'now'
});

$('input[name="item_price[]"], input[name="item_subtotal[]"], input[name="item_discount"], input[name="item_total"]').priceFormat({
    prefix: 'Rp ',
    centsLimit: 0,
    centsSeparator: ',',
    thousandsSeparator: '.',
    allowNegative: true
});

function sum_total(total_element, discount_element)
{
    var total = 0;
    if(total_element.length > 0)
    {
        for(var i=0; i<total_element.length; i++)
        {
            var subtotal = 0;
            var check_val = total_element.eq(i).val();
            
            if((check_val || check_val.length > 0) && check_val.indexOf('Rp') == 0)
            {
                subtotal = parseInt(total_element.eq(i).unmask().slice(1));
            }
            else if(check_val || check_val.length > 0)
            {
                subtotal = check_val;
            }
            total += subtotal;
        }
    }

    var discount = 0;
    if(discount_element.length > 0)
    {
        var curr_discount = discount_element.val();

        if((curr_discount || curr_discount.length > 0)  && curr_discount.indexOf('Rp') == 0)
        {
            discount = parseInt(discount_element.unmask().slice(1));
        }
        else if(curr_discount || curr_discount.length > 0)
        {
            discount = curr_discount;
        }
        
    }
    
    total -= discount;

    return total;
    
}

function check_status_with_price(status, parent)
{
    var price_tag = parent.find('input[name="item_price[]"]');
    var value = price_tag.val();

    if(status == 'BELI')
    {
        if(value.indexOf('-') >= 0)
        {
            value = value.replace('-','');
            price_tag.val(value);
        }
    }
    else if(status == 'BONUS')
    {
        value = 'Rp 0';
        price_tag.val(value);

        var subtotal_tag = parent.find('input[name="item_subtotal[]"]');
        subtotal_tag.val(value);

        var total_element = $('#nota_description').find('input[name="item_subtotal[]"]');
        var discount_element = $('input[name="item_discount"]');
        var total = sum_total(total_element, discount_element);

        var convert_to_currency  = change_int_to_currency(total);
        $('input[name="item_total"]').val(convert_to_currency);

    }
    else if(status == 'RETUR')
    {
        if(value.indexOf('-') < 0)
        {
            value = value.replace('Rp ','Rp -');
            price_tag.val(value);
        }
    }
}

$('#nota_description').on('change', 'select[name="item_status[]"]', function(){
    var current = $(this);
    var parent = current.closest('tr');
    
    var status = current.val();

    check_status_with_price(status,parent);
});

$("#add_row").on('click', function(){
    var prev_row = $(this).closest('tr').prev();

    var html = '<tr>' +
                    '<td>' +
                        '<div class="form-group">' +
                            '<input class="form-control" type="text" name="item_qty[]">' +
                        '</div>' +
                    '</td>' +
                    '<td>' +
                        '<div class="form-group">' +
                            '<select class="form-control" type="text" name="item_status[]">' +
                                '<option value="BELI">Beli</option>' +
                                '<option value="RETUR">Retur</option>' +
                                '<option value="BONUS">Bonus</option>' +
                            '</select>' +
                        '</div>' +
                    '</td>' +
                    '<td>' +
                        '<div class="form-group">' +
                            '<input class="form-control flexdatalist" type="text" name="item_name[]"';
                            if(curr_menu=='invoice')
                            {
                                html += 'data-selection-required="true"';
                            }
            html +=         ' data-min-length="1" data-search-in="nama">' +
                        '</div>' +
                    '</td>' +
                    '<td>' +
                        '<div class="form-group">' +
                            '<input class="form-control" type="text" name="item_price[]" value="0">' +
                        '</div>' +
                    '</td>' +
                    '<td>' +
                        '<div class="form-group">' +
                            '<input class="form-control" type="text" name="item_subtotal[]" value="0" readonly>' +
                        '</div>' +
                    '</td>' +
                    '<td class="text-right">' +
                        '<button class="btn btn-danger delete_row" type="button"><i class="glyphicon glyphicon-minus"></i> Batal</button>' +
                    '</td>' +
                '</tr>';
    
    prev_row.after(html);

    setTimeout(function () {
        $('.flexdatalist').filter(function(){
            if(!this.value)
            {
                add_flexdata_instance($(this),curr_click);
            }
        });
    }, 100);

    $('input[name="item_price[]"], input[name="item_subtotal[]"]').priceFormat({
        prefix: 'Rp ',
        centsLimit: 0,
        centsSeparator: ',',
        thousandsSeparator: '.',
        allowNegative: true
    });
});

$("#nota_description").on('click','.delete_row' , function(){
        
    var curr_row = $(this).closest('tr');
    curr_row.remove();

    var total_element = $('#nota_description').find('input[name="item_subtotal[]"]');
    var discount_element = $('input[name="item_discount"]');
    var total = sum_total(total_element, discount_element);

    convert_to_currency  = change_int_to_currency(total);
    $('input[name="item_total"]').val(convert_to_currency);
});

$('input[name="item_discount"]').on('keyup',function(){
    var total_element = $('#nota_description').find('input[name="item_subtotal[]"]');
    var discount_element = $(this);
    var total = sum_total(total_element, discount_element);

    convert_to_currency  = change_int_to_currency(total);
    $('input[name="item_total"]').val(convert_to_currency);
});

$('#nota_description').on('keyup','input[name="item_price[]"]',function(){
    var curr_element = $(this);

    var curr_price = 0;
    var curr_row = $(this).closest('tr');
    
    // check status
    var status = curr_row.find('select[name="item_status[]"]').val();
    check_status_with_price(status,curr_row);

    if(curr_element.val().indexOf('Rp') == 0)
        curr_price = parseInt(curr_element.unmask().slice(1));
    else
        curr_price = curr_element.val();

    var curr_qty = curr_row.find('input[name="item_qty[]"]').val();

    if(!curr_qty || curr_qty.length == 0)
        curr_qty = 0;
    
    var subtotal = curr_qty * curr_price;
    
    var convert_to_currency  = change_int_to_currency(subtotal);

    curr_row.find('input[name="item_subtotal[]"]').val(convert_to_currency);

    var total_element = $('#nota_description').find('input[name="item_subtotal[]"]');
    var discount_element = $('input[name="item_discount"]');
    var total = sum_total(total_element, discount_element);
    
    convert_to_currency  = change_int_to_currency(total);
    $('input[name="item_total"]').val(convert_to_currency);
});

$('#nota_description').on('keyup','input[name="item_qty[]"]',function(){
    var curr_element = $(this);

    var curr_qty = curr_element.val();

    var curr_row = $(this).closest('tr');

    // check status
    var status = curr_row.find('select[name="item_status[]"]').val();
    check_status_with_price(status,curr_row);

    var curr_price_element = curr_row.find('input[name="item_price[]"]').val();
    
    var curr_price = 0;
    if((curr_price_element || curr_price_element.length > 0) && curr_price_element.indexOf('Rp') == 0)
    {
        curr_price = parseInt(curr_row.find('input[name="item_price[]"]').unmask().slice(1));
    }
    
    var subtotal = curr_qty * curr_price;
    var convert_to_currency  = change_int_to_currency(subtotal);

    curr_row.find('input[name="item_subtotal[]"]').val(convert_to_currency);

    var total_element = $('#nota_description').find('input[name="item_subtotal[]"]');
    var discount_element = $('input[name="item_discount"]');
    var total = sum_total(total_element, discount_element);

    convert_to_currency  = change_int_to_currency(total);
    $('input[name="item_total"]').val(convert_to_currency);
});

// Save Invoice
$('#modal_invoice').on('click', '#save_invoice, #modal_submit', function(){
    id = $(this).attr('id');
    $('#modal_invoice').modal('hide');

    var url = $('#url_save_invoice').attr('data-url');
    var qty_count = $('input[name="item_qty[]"]').length;
    var price_element = $('input[name="item_price[]"]');
    var subtotal_element = $('input[name="item_subtotal[]"]');
    var name_element = $('input[name="item_name[]"]');
    var status_element = $('select[name="item_status[]"]');

    for(var i = 0; i<qty_count; i++)
    {
        if(!name_element.eq(i).val() || name_element.eq(i).val().length <=0)
        {
            var status = new Object();
            status.code = 500;
            status.message = 'Nama Barang tidak boleh kosong';
            get_alert_status_object(status);
            return false;
        }

        var indexOf_val = price_element.eq(i).val();

        if(indexOf_val.indexOf('Rp') >= 0)
        {
            before_submit_change_to_int(price_element.eq(i));
        }

        indexOf_val = subtotal_element.eq(i).val();
        if(indexOf_val.indexOf('Rp') >= 0)
        {
            before_submit_change_to_int(subtotal_element.eq(i));
        }
        
        if(status_element.eq(i).val() != 'BONUS' && price_element.eq(i).val() == 0)
        {
            var status = new Object();
            status.code = 500;
            status.message = 'Harga Barang tidak boleh 0';
            get_alert_status_object(status);
            return false;
        }
        
    }

    if($('input[name="item_discount"]').val().indexOf('Rp') >= 0)
        before_submit_change_to_int($('input[name="item_discount"]'));

    if($('input[name="item_total"]').val().indexOf('Rp') >= 0)
        before_submit_change_to_int($('input[name="item_total"]'));
    
    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        url: url,
        data: $('#form_nota').serialize(),
        success: function(data) {
            // general_function
            get_alert_status_object(data);
            $('#form_nota')[0].reset();
            var has_append_row = $('#modal_invoice').find('.delete_row');
            if(has_append_row.length > 0)
            {
                has_append_row.each(function(){
                    $(this).closest('tr').remove();
                });
            }
            $('#archive_table').DataTable().ajax.reload();
        },
        error: function(e) {
            // general_function
            get_alert_error();
        }
    });
});

function add_flexdata_instance(curr_flexdatalist, click_button = '')
{
    if(click_button == 'invoice')
    {
        var flexdataInstance = curr_flexdatalist.flexdatalist({
            minLength: 1,
            selectionRequired: true,
            visibleProperties: ["nama","status","stok"],
            valueProperty: 'nama',
            searchIn: 'name',
            data : $('#url_all_product').attr('data-url'),
            searchByWord: true
        });

        flexdataInstance.on('select:flexdatalist',function(event, data){
            var parent = $(this).closest('tr');
            var curr_price_tag = parent.find('input[name="item_price[]"]');

            var letterToUse = data.harga;
            var e = $.Event("keyup");
            e.which=e.keyCode=letterToUse.charCodeAt();
            curr_price_tag.val(data.harga).trigger(e);
            
        });
    }
    else if(click_button == 'archive')
    {
        curr_flexdatalist.flexdatalist({
            minLength: 1,
            visibleProperties: ["nama","status","stok"],
            valueProperty: 'nama',
            searchIn: 'name',
            data : $('#url_all_product').attr('data-url'),
            searchByWord: true,
        });
    }
}

function format(d){
    // 'd' is the original data object for the row
    var html = '<table class="table" width="100%">' +
                    '<thead>' +
                        '<th width="5%">Banyak</th>' +
                        '<th width="15%" class="mdl-data-table__cell--non-numeric">Status</th>' +
                        '<th width="40%" class="mdl-data-table__cell--non-numeric">Nama Barang</th>' +
                        '<th width="20%">Harga</th>' +
                        '<th width="20%">Jumlah</th>' +
                    '</thead>' +
                    '<tbody>';

    if(d.detail && typeof d.detail == 'object')
    {
        if(d.detail.list_product && d.detail.list_product.length)
        {
            var product = d.detail.list_product;
            for(var i = 0; i < product.length; i++)
            {
                html += '<tr>' +
                            '<td>' + product[i].QTY + '</td>' +
                            '<td class="mdl-data-table__cell--non-numeric">' + product[i].STATUS_PRODUCT + '</td>' +
                            '<td class="mdl-data-table__cell--non-numeric">' + product[i].NAMA_PRODUCT + '</td>' +
                            '<td>' + change_int_to_currency(parseInt(product[i].HARGA_PRODUCT)) + '</td>' +
                            '<td>' + change_int_to_currency(parseInt(product[i].SUBTOTAL_PRODUCT)) + '</td>' +
                        '</tr>';
            }
        }
        else
        {
            html += '<tr><td colspan="5">No Data Available</td></tr>';
        }
    }
    else
    {
        html += '<tr><td colspan="5">No Data Available</td></tr>';
    }

    html +=         '</tbody>' +
                    '<tfoot>' +
                        '<tr>' +
                            '<td class="text-right vertical-middle" colspan="4">' +
                                '<strong>DISKON</strong>' +
                            '</td>' +
                            '<td>' +
                                '<div class="form-group">' +
                                    d.diskon +
                                '</div>' +
                            '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="text-right vertical-middle" colspan="4">' +
                                '<strong>TOTAL</strong>' +
                            '</td>' +
                            '<td>' +
                                '<div class="form-group">' +
                                    d.total +
                                '</div>' +
                            '</td>' +
                        '</tr>' +
                        '<tr>' +
                            '<td class="text-right vertical-middle" colspan="4">' +
                                '<strong>SELLER</strong>' +
                            '</td>' +
                            '<td>' +
                                '<div class="form-group">' +
                                    d.seller +
                                '</div>' +
                            '</td>' +
                        '</tr>' +
                    '</tfoot>' +
                '</table>';

    return html;
};

var dataTable = $('#archive_table').DataTable({
    "deferRender"    : true,
    "scrollX"        : true,
    "scrollCollapse" : true,
    "scroller"       : true,
    "ajax"          : {
        "url" : $('#url_all_archive').attr('data-url'),
        "type": "POST",
        "headers" : {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        }
    },
    "columns"       : [
            {"data"   : 'tanggal', type: "date"},
            {"data"   : 'nama'},
            {"data"   : 'ponsel', searchable  : false, sortable   : false},
            {"data"   : 'alamat', searchable  : false, sortable   : false},
            {"data"   : 'status', render:function(data,type,row,meta){
                var html = '';
                if(data == 'LUNAS')
                    html = '<p class="label label-success">' + data + '</p>';
                else   
                    html = '<p class="label label-danger">' + data + '</p>';
                return html;
            }},
            {"data"   : 'id', "class" : "text-center" ,searchable  : false, sortable   : false, render:function(data,type,row,meta){
                var html = '<button class="btn btn-sm btn-default expand-archive" data-value="' + (meta.row +1) + '"><i class="glyphicon glyphicon-chevron-down"></i> Detail</button>';

                if(row.status != 'LUNAS')
                    html += '&nbsp;&nbsp;' +
                            '<button class="btn btn-sm btn-success edit_status" data-target="' + data + '" data-value="' + (meta.row +1) + '"><i class="glyphicon glyphicon-ok"></i> Lunas</button>';

                var print_url = $('#url_print_invoice').attr('data-url') + '/';
                html += '&nbsp;&nbsp;' +
                        '<a class="btn btn-sm btn-default print_invoice" target="_blank" href="' + print_url + data + '"><i class="glyphicon glyphicon-print"></i> Print</a>';

                return html;
            }}
    ],
    "columnDefs"    : [
        {
            targets : [0,1,2,4,5],
            "class" : "mdl-data-table__cell--non-numeric"
        },
        {
            targets : [3],
            "class" : "mdl-data-table__cell--non-numeric text-wrap"
        }
    ],
    "order"         : [[ 0, 'desc' ],[1,'asc']]
});

$('#archive_table').on('click', '.expand-archive', function(){
    var tr = $(this). closest('tr');
    var row = dataTable.row(tr);
    var button = $(this);

    if(row.child.isShown())
    {
        // this row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
        button.find('i').removeClass('glyphicon-chevron-up');
        button.find('i').addClass('glyphicon-chevron-down');
    }
    else
    {
        // open this row
        row.child(format(row.data())).show();
        tr.addClass('shown');
        button.find('i').removeClass('glyphicon-chevron-down');
        button.find('i').addClass('glyphicon-chevron-up');
    }
});

$('#add_new_archive, #create_invoice').on('click', function(){
    var curr_id = $(this).attr('id');

    var input_target_save = $('input[name="target_save"]');
    var modal_title = $('#modal_title');
    var modal_footer = $('#modal_footer');
    var html = '';

    if(curr_id.indexOf('archive') >=0)
    {
        curr_click = 'archive';
        add_flexdata_instance($('.flexdatalist'),curr_click);

        input_target_save.val('old');
        modal_title.text('Tambah Arsip');
        html = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                '<button type="button" class="btn btn-primary" id="modal_submit" data-target="save">Save changes</button>';
        modal_footer.html(html);
    }
    else
    {
        curr_click = 'invoice';
        add_flexdata_instance($('.flexdatalist'),curr_click);
        input_target_save.val('new');
        modal_title.text('Nota Baru');
        html = '<button class="btn" type="button" id="save_invoice">Simpan Nota</button>';
        modal_footer.html(html);
    }

    $('#modal_invoice').modal();
});

$('#archive_table').on('click','.edit_status', function(){
    var id = $(this).attr('data-target');

    $.ajax({
        type: 'POST',
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        url: $('#url_update_archive').attr('data-url'),
        data: {'id' : id, 'status' : 'lunas'},
        success: function(data) {
            // general_function
            get_alert_status_object(data);

            $('#archive_table').DataTable().ajax.reload();
            
        },
        error: function(e) {
            // general_function
            get_alert_error();
        }
    });
    
});