$(document).ready(function(){
    function format(d){
        var menu = '';
        try {
            menu = JSON.parse(d.menu);
        } catch (error) {
            menu = d.menu;
        }
        
        var html = '<div class="row mdl-data-table__cell--non-numeric">';

        if(Array.isArray(menu))
        {
            var length = menu.length;
            var data_per_column = Math.ceil(length/4);
            var count_column = 12/Math.ceil(length/data_per_column);
            var start = 1;
            for(var i=0; i<length; i++)
            {
                if(start == 1)
                    html += '<div class="col-lg-' + count_column + ' col-md-' + count_column + '">';

                html += '<p class="inline">' + menu[i] + '</p><br>';

                if(start == data_per_column)
                {
                    html += '</div>';
                    start = 1;
                    continue;
                }
                start ++;
                    
            }
        }
        else
        {
            html += '<div class="col-lg-12 col-md-12"><p class="inline">' + menu + '</p></div>';
        }
        
        html += '</div>';
        return html;
    };

    var dataTable = $('#user_table').DataTable({
        "deferRender"    : true,
        "scrollX"        : true,
        "scrollCollapse" : true,
        "scroller"       : true,
        "ajax"          : {
            "url" : $('#url_all_user').attr('data-url'),
            "type": "POST",
            "headers" : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            }
        },
        "columns"       : [
                {"data"   : 'id', searchable  : false, render:function(data,type,row,meta){
                    return '<span data-target="' + data + '">' + (meta.row +1) + '</span>';
                }},
                {"data"   : 'username'},
                {"data"   : 'status', render:function(data,type,row,meta){
                    var html = '';
                    if(data == 'ACTIVE')
                        html = '<p class="label label-success">' + data + '</p>';
                    else   
                        html = '<p class="label label-danger">' + data + '</p>';
                    return html;
                }},
                {"data"   : 'role'},
                {"data"   : 'menu',searchable  : false, sortable   : false, render:function(data,type,row,meta){
                    var find = ["[\"", "\",\"", "\"]"];
                    var replace = ["", ";", ""];

                    for(var i = 0; i< find.length; i++)
                    {
                        data = data.split(find[i]).join(replace[i]);
                    }
                    
                    var html = '<button class="btn btn-sm btn-default expand-menu" data-value="' + (meta.row +1) + '"><i class="glyphicon glyphicon-chevron-down"></i> Show Menu</button>';
                    html += "<span class='hide' data-value='" + data + "'></span>";
                    return html;
                }},
                {"data"   : 'last_login'},
                {"data"   : '', "class" : "text-center" ,searchable  : false, sortable   : false, render:function(data,type,row,meta){
                    var html ='<button class="btn btn-sm btn-info edit_user" data-value="' + (meta.row +1) + '"><i class="glyphicon glyphicon-pencil"></i> Edit</button>' +
                                '&nbsp;&nbsp;' +
                                '<button class="btn btn-sm btn-danger delete_user" data-value="' + (meta.row +1) + '"><i class="glyphicon glyphicon-trash"></i> Delete</button>' ;
                    return html;
                }}
        ],
        "columnDefs"    : [
            {
                targets : [1,2,3,4],
                "class" : "mdl-data-table__cell--non-numeric"
            }
        ],
        "order"         : [[ 0, 'asc' ],[1,'asc']]
    });

    $('#user_table').on('click', '.expand-menu', function(){
        var tr = $(this). closest('tr');
        var row = dataTable.row(tr);
        var button = $(this);
    
        if(row.child.isShown())
        {
            // this row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
            button.find('i').removeClass('glyphicon-chevron-up');
            button.find('i').addClass('glyphicon-chevron-down');
        }
        else
        {
            // open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
            button.find('i').removeClass('glyphicon-chevron-down');
            button.find('i').addClass('glyphicon-chevron-up');
        }
    });

    $('#add_new_user').on('click',function(){
        $('#modal_title').text('User Baru');
        $('#modal_submit').attr('data-target','add');

        var tag = $('#modal_user').find('input[name="username"]');

        if(tag.prop('disabled'))
        {
            tag.prop('disabled',false);
        }

        tag = $('#modal_user').find('input[name="password"]').parent();
        if(tag.hasClass('hide'))
        {
            tag.removeClass('hide');
        }

        //empty all value
        $('#submit_user')[0].reset();
        $('input[name="id_user"]').val('');
        $('#modal_user').modal();
    });

    // add edit
    $('#modal_submit').on('click', function(){
        var submit = $('#modal_submit').attr('data-target');
        $('#modal_user').modal('hide');

        var url = '';

        if(submit == 'add')
                url =$('#insert_user').attr('data-url');
        else if(submit == 'edit')
            url =$('#update_user').attr('data-url');
        
        if(url)
        {
            $.ajax({
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                url: url,
                data: $('#submit_user').serialize(),
                success: function(data) {
                    // general_function
                    get_alert_status_object(data);

                    $('#user_table').DataTable().ajax.reload();
                },
                error: function(e) {
                    //general_function
                    get_alert_error();
                }
            });
        }
        else
        {
            return false;
        }
    });

    $('#user_table').on('click', '.edit_user', function(){
        var selected_row = $(this).closest('tr').find('td');
        
        $('#modal_title').text('Edit User');
        $('#modal_submit').attr('data-target','edit');

        //populate the modal
        var index = 0;
        $('#modal_user *').filter('.form-control').each(function(){
            var current_tag = $(this);
            var current_val = selected_row.eq(index);

            if(index == 0)
                current_val = current_val.find('span').attr('data-target');
            else
                current_val = current_val.text();
                
            if(current_tag.attr('name') == 'username')
            {
                current_tag.prop('disabled',true);
                current_tag.val(current_val);
            }
            else if(current_tag.attr('name') == 'password')
            {
                current_tag.parent().addClass('hide');
            }
            else
                current_tag.val(current_val);
            
            index++;
        });

        var current_column = selected_row.eq(index);
        var current_val = current_column.find('span').attr('data-value');
        
        $('#modal_user *').filter('.form-check-input').each(function(){
            var current_tag = $(this);
            var curr_tag_val = current_tag.val();
            
            var find = current_val.indexOf(curr_tag_val);

            if(find >= 0)
            {
                current_tag.prop('checked',true);
            }
            else
            {
                current_tag.prop('checked',false);
            }
        });

        $('#modal_user').modal();
    });

    $('#user_table').on('click', '.delete_user', function(){
        //empty all value
        $('#submit_user')[0].reset();
        $('input[name="id_user"]').val('');

        var selected_row = $(this).closest('tr').find('td');

        //get the id to input
        var selected_id = selected_row.find('span').attr('data-target');
        $('input[name="id_user"]').val(selected_id);

        var username = selected_row.eq(1).text();

        $('#warning_title').text('Hapus User');
        $('#warning_content').text('Apakah Anda yakin ingin menghapus ' + username + '?');
        $('#modal_warning').modal();
    });

    // delete
    $('#submit_warning').on('click', function(){
        $('#modal_warning').modal('hide');

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: $('#delete_user').attr('data-url'),
            data: {'id_user': $('input[name="id_user"]').val()},
            success: function(data) {
                // general_function
                get_alert_status_object(data);

                $('#user_table').DataTable().ajax.reload();
            },
            error: function(e) {
                //general_function
                get_alert_error();
            }
        });
    });

    $('#modal_user').on('change','select[name="role"]', function(){
        var current_val = $(this).val();

        var all_checkbox = $('#modal_user').find('.form-check-input').parent();

        if(current_val == 'ADMIN')
        {
            all_checkbox.removeClass('hide');
        }
        else if(current_val == 'MEMBER')
        {
            var allowed = ['PRODUK','ARSIP'];
            $('#modal_user *').filter('.form-check-input').each(function(){
                var current_tag = $(this);
                var curr_tag_val = current_tag.val();
                
                if(allowed.indexOf(curr_tag_val) < 0)
                {
                    current_tag.parent().addClass('hide');
                }
                
            });
        }
        
    });

});