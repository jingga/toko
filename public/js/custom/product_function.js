$(document).ready(function(){
    var current_role = $('#curr_role').attr('data-value');

    if(current_role != 'MEMBER')
    {
        $('input[name="modal_product"]').parent().removeClass('hide');
    }
    else
    {
        $('input[name="modal_product"]').parent().addClass('hide');
    }

    $('input[name="modal_product"], input[name="harga_product"]').priceFormat({
        prefix: 'Rp ',
        centsLimit: 0,
        centsSeparator: ',',
        thousandsSeparator: '.'
    });

    if(current_role != 'MEMBER')
    {
        $('#product_table').DataTable({
            "deferRender"    : true,
            "scrollX"        : true,
            "scrollCollapse" : true,
            "scroller"       : true,
            "progress"       : true,
            "ajax"          : {
                "url" : $('#url_all_product').attr('data-url'),
                "type": "POST",
                "headers" : {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            },
            "columns"       : [
                    {"data"   : 'id', searchable  : false, render:function(data,type,row,meta){
                        return '<span data-target="' + data + '">' + (meta.row +1) + '</span>';
                    }},
                    {"data"   : 'nama'},
                    {"data"   : 'stok', searchable  : false},
                    {"data"   : 'modal', searchable  : false},
                    {"data"   : 'harga', searchable  : false},
                    {"data"   : 'status', render:function(data,type,row,meta){
                        var html = '';
                        if(data == 'AVAILABLE')
                            html = '<p class="label label-success">' + data + '</p>';
                        else if(data == 'PENDING')
                            html = '<p class="label label-warning">' + data + '</p>';
                        else   
                            html = '<p class="label label-danger">' + data + '</p>';
                        return html;
                    }},
                    {"data"   : 'lokasi', searchable  : false, sortable   : false},
                    {"data"   : '', "class" : "text-center" ,searchable  : false, sortable   : false, render:function(data,type,row,meta){
                        var html = '<button class="btn btn-sm btn-success stock_in"><i class="glyphicon glyphicon-import"></i> In</button>' +
                                    '&nbsp;' +
                                    '<button class="btn btn-sm btn-warning stock_out"><i class="glyphicon glyphicon-export"></i> Out</button>' +
                                    '&nbsp;' +
                                    '<button class="btn btn-sm btn-info edit_product"><i class="glyphicon glyphicon-pencil"></i> Edit</button>' +
                                    '&nbsp;' +
                                    '<button class="btn btn-sm btn-danger delete_product"><i class="glyphicon glyphicon-trash"></i> Delete</button>' +
                                    '&nbsp;' +
                                    '<button class="btn btn-sm btn-default stock_history"><i class="glyphicon glyphicon-list-alt"></i> History</button>';
                        return html;
                    }}
            ],
            "columnDefs"    : [
                {
                    targets : [1,6],
                    "class" : "mdl-data-table__cell--non-numeric"
                }
            ],
            "order"         : [[ 0, 'asc' ]]
        });
    }
    else
    {
        $('#product_table').DataTable({
            "deferRender"    : true,
            "scrollX"        : true,
            "scrollCollapse" : true,
            "scroller"       : true,
            "ajax"          : {
                "url" : $('#url_all_product').attr('data-url'),
                "type": "POST",
                "headers" : {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            },
            "columns"       : [
                    {"data"   : 'id', searchable  : false, render:function(data,type,row,meta){
                        return '<span data-target="' + data + '">' + (meta.row +1) + '</span>';
                    }},
                    {"data"   : 'nama'},
                    {"data"   : 'stok', searchable  : false},
                    {"data"   : 'harga', searchable  : false},
                    {"data"   : 'status', render:function(data,type,row,meta){
                        var html = '';
                        if(data == 'AVAILABLE')
                            html = '<p class="label label-success">' + data + '</p>';
                        else if(data == 'PENDING')
                            html = '<p class="label label-warning">' + data + '</p>';
                        else   
                            html = '<p class="label label-danger">' + data + '</p>';
                        return html;
                    }},
                    {"data"   : 'lokasi', searchable  : false, sortable   : false},
                    {"data"   : '', "class" : "text-center" ,searchable  : false, sortable   : false, render:function(data,type,row,meta){
                        var html = '<button class="btn btn-sm btn-success stock_in"><i class="glyphicon glyphicon-import"></i> In</button>' +
                                    '&nbsp;' +
                                    '<button class="btn btn-sm btn-warning stock_out"><i class="glyphicon glyphicon-export"></i> Out</button>' +
                                    '&nbsp;' +
                                    '<button class="btn btn-sm btn-default stock_history"><i class="glyphicon glyphicon-list-alt"></i> History</button>';
                        return html;
                    }}
            ],
            "columnDefs"    : [
                {
                    targets : [1,5],
                    "class" : "mdl-data-table__cell--non-numeric"
                }
            ],
            "order"         : [[ 0, 'asc' ]]
        });
    }

    function loadHistory(id_product, product_name)
    {
        $('#history_section').removeClass('hide');

        $('html, body').animate({
            scrollTop: ($('#history_section').first().offset().top)
        },500);

        $('#history_title').text(product_name);

        $('#history_table').DataTable({
            "deferRender"    : true,
            "scrollX"        : true,
            "scrollCollapse" : true,
            "scroller"       : true,
            "searchable"     : false,
            "sortable"       : false,
            "ajax"          : {
                "url" : $('#url_history').attr('data-url'),
                "data": {'id_product' : id_product},
                "type": "POST",
                "headers" : {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            },
            "columns"       : [
                    {"data"   : 'tanggal', searchable : false, sortable: false},
                    {"data"   : 'keterangan', searchable : false, sortable: false},
                    {"data"   : 'before', searchable : false, sortable: false},
                    {"data"   : 'qty', searchable : false, sortable: false, render:function(data,type,row,meta){
                        var html = '<p class="';

                        if(data < 0)
                            html += 'decrease';
                        else
                            html += 'increase';

                        html += '">' + data + '</p>';
                        return html;
                    }},
                    {"data"   : 'after', searchable : false, sortable: false}
            ],
            "columnDefs"    : [
                {
                    targets : [0,1],
                    "class" : "mdl-data-table__cell--non-numeric"
                }
            ]
        });
    }

    $('#add_new_product').on('click',function(){
        $('#modal_title').text('Tambah Produk');
        $('#modal_submit').attr('data-target','add');

        //empty all value
        $('#submit_product')[0].reset();
        $('input[name="id_product"]').val('');
        $('#modal_product').modal();
    });

    $('#product_table').on('click', '.edit_product', function(){
        var selected_row = $(this).closest('tr').find('td');
        
        $('#modal_title').text('Edit Produk');
        $('#modal_submit').attr('data-target','edit');

        //populate the modal
        var index = 0;
        $('#modal_product *').filter('.form-control').each(function(){
            var current_tag = $(this);
            var current_val = selected_row.eq(index);

            if(index == 0)
                current_val = current_val.find('span').attr('data-target');
            else
                current_val = current_val.text();
                
            current_tag.val(current_val);
            index++;
        });

        $('#modal_product').modal();
    });

    $('#product_table').on('click', '.delete_product', function(){
        //empty all value
        $('#submit_product')[0].reset();
        $('input[name="id_product"]').val('');

        var selected_row = $(this).closest('tr').find('td');

        //get the id to input
        var selected_id = selected_row.find('span').attr('data-target');
        $('input[name="id_product"]').val(selected_id);

        var product_name = selected_row.eq(1).text();

        $('#warning_title').text('Hapus Produk');
        $('#warning_content').text('Apakah Anda yakin ingin menghapus ' + product_name + '?');
        $('#modal_warning').modal();
    });

    $('#product_table').on('click', '.stock_history', function(){
        var selected_row = $(this).closest('tr').find('td');
        var curr_product_name = selected_row.eq(1).text();
        var current_product_id = selected_row.eq(0).find('span').attr('data-target');

        if($('#history_section').hasClass('hide'))
            loadHistory(current_product_id, curr_product_name);
        else
        {
            $('#history_table').DataTable().clear().draw().destroy();
            $('#history_section').addClass('hide');
            loadHistory(current_product_id, curr_product_name);
        }
    });

    $('#history_section').on('click', '.close_table_detail', function(){
        $('html, body').animate({
            scrollTop: 0
        },500);

        $('#history_table').DataTable().clear().draw().destroy();
        $('#history_section').addClass('hide');
        
    });

    $('#product_table').on('click', '.stock_in', function(){
        var selected_row = $(this).closest('tr').find('td');
        var curr_product_name = selected_row.eq(1).text();
        
        $('#modal_history_title').text('Masuk Stok ' + curr_product_name);

        var current_product_id = selected_row.eq(0).find('span').attr('data-target');

        $('input[name="history_id_product"]').val(current_product_id);
        $('input[name="history_action"]').val('in');

        $('#modal_history').modal();
    });

    $('#product_table').on('click', '.stock_out', function(){
        var selected_row = $(this).closest('tr').find('td');
        var curr_product_name = selected_row.eq(1).text();
        
        $('#modal_history_title').text('Keluar Stok ' + curr_product_name);

        var current_product_id = selected_row.eq(0).find('span').attr('data-target');

        $('input[name="history_id_product"]').val(current_product_id);
        $('input[name="history_action"]').val('out');

        $('#modal_history').modal();
    });

    $('#modal_history_submit').on('click', function(){
        $('#modal_history').modal('hide');

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: $('#update_stock_product').attr('data-url'),
            data: $('#submit_history').serialize(),
            success: function(data) {
                // general_function
                get_alert_status_object(data);

                $('#product_table').DataTable().ajax.reload();
                $('#history_table').DataTable().clear().draw().destroy();
            },
            error: function(e) {
                //general_function
                get_alert_error();
            }
        });
    });

    // add edit
    $('#modal_submit').on('click', function(){
        var submit = $('#modal_submit').attr('data-target');
        var url = '';
        $('#modal_product').modal('hide');
        
        if(submit == 'add')
        {
            // currency modal
            before_submit_change_to_int($('input[name="modal_product"]'));

            // currency harga
            before_submit_change_to_int($('input[name="harga_product"]'));

            url = $('#insert_product').attr('data-url');
        }
        else if (submit == 'edit')
        {
            // currency modal
            before_submit_change_to_int($('input[name="modal_product"]'));

            // currency harga
            before_submit_change_to_int($('input[name="harga_product"]'));

            url = $('#update_product').attr('data-url');
        }
        else
        {
            return false;
        }

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: url,
            data: $('#submit_product').serialize(),
            success: function(data) {
                // general_function
                get_alert_status_object(data);

                $('#product_table').DataTable().ajax.reload();
                $('#history_table').DataTable().clear().draw().destroy();
            },
            error: function(e) {
                //general_function
                get_alert_error();
            }
        });
    });

    // delete
    $('#submit_warning').on('click', function(){
        $('#modal_warning').modal('hide');

        $.ajax({
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            },
            dataType: 'json',
            url: $('#delete_product').attr('data-url'),
            data: {'id_product': $('input[name="id_product"]').val()},
            success: function(data) {
                // general_function
                get_alert_status_object(data);

                $('#product_table').DataTable().ajax.reload();
                $('#history_table').DataTable().clear().draw().destroy();
            },
            error: function(e) {
                //general_function
                get_alert_error();
            }
        });
    });

});