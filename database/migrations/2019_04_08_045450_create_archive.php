<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive', function (Blueprint $table) {
            $table->uuid('ID_ARCHIVE');
            $table->primary('ID_ARCHIVE');

            $table->uuid('ID_TOKO');
            $table->foreign('ID_TOKO')->references('ID_TOKO')->on('toko')->onDelete('cascade');
            
            $table->datetime('TANGGAL')->nullable();
            $table->string('NAMA_KONSUMEN')->default('CASH');
            $table->text('ALAMAT_KONSUMEN')->nullable();
            $table->string('PONSEL_KONSUMEN')->nullable();
            $table->text('DETAIL_PRODUCT')->nullable();
            $table->integer('DISCOUNT')->default(0)->nullable();
            $table->string('SELLER')->nullable();
            $table->string('STATUS')->default('LUNAS');
            $table->datetime('CREATED_AT')->nullable();
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive');
    }
}
