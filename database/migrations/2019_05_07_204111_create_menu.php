<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');

            $table->string('MENU_CODE');
            $table->string('MENU_DESCRIPTION');
            $table->string('MENU_URL');
            $table->string('MENU_ICON');
            $table->string('STATUS')->nullable();
            $table->datetime('CREATED_AT')->nullable();
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
    }
}
