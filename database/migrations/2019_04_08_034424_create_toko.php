<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Webpatser\Uuid\Uuid;

class CreateToko extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toko', function (Blueprint $table) {
            $table->uuid('ID_TOKO');
            $table->primary('ID_TOKO');
            $table->string('NAMA_TOKO');
            $table->string('URL_TOKO');
            $table->string('LOGO_TOKO')->nullable();
            $table->string('STATUS')->nullable();
            $table->string('CREATED_BY')->default('SYSTEM');
            $table->string('UPDATED_BY')->default('SYSTEM');
            $table->datetime('CREATED_AT')->nullable();
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toko');
    }
}
