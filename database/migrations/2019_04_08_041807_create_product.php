<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->uuid('ID_PRODUCT');
            $table->primary('ID_PRODUCT');

            $table->uuid('ID_TOKO');
            $table->foreign('ID_TOKO')->references('ID_TOKO')->on('toko')->onDelete('cascade');

            $table->string('NAMA_PRODUCT');
            $table->integer('STOCK_PRODUCT')->default(0);
            $table->integer('MODAL_PRODUCT')->default(0);
            $table->integer('HARGA_PRODUCT')->default(0);
            $table->string('LOKASI_PRODUCT')->nullable();
            $table->string('STATUS')->nullable();
            $table->datetime('CREATED_AT')->nullable();
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
