<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_history', function (Blueprint $table) {
            $table->uuid('ID_STOCK');
            $table->primary('ID_STOCK');

            $table->uuid('ID_TOKO');
            $table->foreign('ID_TOKO')->references('ID_TOKO')->on('toko')->onDelete('cascade');

            $table->uuid('ID_PRODUCT');
            $table->foreign('ID_PRODUCT')->references('ID_PRODUCT')->on('product')->onDelete('cascade');

            $table->integer('SEQUENCE');
            $table->datetime('TANGGAL');
            $table->text('DESCRIPTION');
            $table->integer('QTY');
            $table->integer('STOCK_BEFORE');
            $table->integer('STOCK_AFTER');
            $table->datetime('CREATED_AT')->nullable();
            $table->datetime('UPDATED_AT')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_history');
    }
}
