<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->uuid('ID_KARYAWAN');
            $table->primary('ID_KARYAWAN');

            $table->uuid('ID_TOKO');
            $table->foreign('ID_TOKO')->references('ID_TOKO')->on('toko')->onDelete('cascade');
            
            $table->string('NAMA_KARYAWAN');
            $table->string('STATUS')->nullable();
            $table->integer('SALARY')->nullable()->default(0);
            $table->string('SALARY_TYPE')->nullable();
            $table->string('SALARY_DUE')->nullable();
            $table->integer('BONUS_MONTHLY')->nullable()->default(0);
            $table->integer('BONUS')->nullable()->default(0);
            $table->datetime('CREATED_AT')->nullable();
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
