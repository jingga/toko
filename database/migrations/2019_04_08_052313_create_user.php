<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->uuid('ID_USER');
            $table->primary('ID_USER');

            $table->uuid('ID_TOKO');
            $table->foreign('ID_TOKO')->references('ID_TOKO')->on('toko')->onDelete('cascade');
            
            $table->string('USERNAME');
            $table->string('PASSWORD');
            $table->string('ROLE')->nullable();
            $table->string('STATUS')->nullable();
            $table->text('MENU');
            $table->string('CREATED_BY')->default('SYSTEM');
            $table->string('UPDATED_BY')->default('SYSTEM');
            $table->datetime('LAST_LOGIN')->nullable();
            $table->datetime('CREATED_AT')->nullable();
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
