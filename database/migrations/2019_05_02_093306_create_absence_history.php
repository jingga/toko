<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsenceHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absence_history', function (Blueprint $table) {
            $table->uuid('ID_ABSENCE');
            $table->primary('ID_ABSENCE');

            $table->uuid('ID_TOKO');
            $table->foreign('ID_TOKO')->references('ID_TOKO')->on('toko')->onDelete('cascade');
            
            $table->uuid('ID_KARYAWAN');
            $table->foreign('ID_KARYAWAN')->references('ID_KARYAWAN')->on('karyawan')->onDelete('cascade');

            $table->datetime('TANGGAL')->nullable();
            $table->string('STATUS')->nullable();
            $table->datetime('CREATED_AT')->nullable();
            $table->datetime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absence_history');
    }
}
