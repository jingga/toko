<form id="submit_income_outcome">
        <div class="modal fade" id="modal_income_outcome" tabindex="-1" role="dialog" aria-labelledby="labelModal_Income_Outcome" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modal_title">Title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input class="form-control" type="text" name="tanggal">
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input class="form-control" type="text" name="keterangan">
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input class="form-control" type="text" name="jumlah">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="modal_submit" data-target="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>