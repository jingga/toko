<?php use App\Helpers\Helper;?>
<!DOCTYPE html>
<html xmlns="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- BOOTSTRAP STYLES-->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="{{asset('css/basic.css')}}" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    
    <link href="{{asset('css/print.css')}}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="{{asset('css/material.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.material.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.flexdatalist.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-datepicker3.css')}}">
</head>
<body>
    <div class="row pad-all container-print">
        <div class="col-print-12 col-sm-12">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-print-4">
                    <img class="logo" src="{{$toko->LOGO_TOKO ? asset($toko->LOGO_TOKO) : asset('/images/no-thumbnail.png')}}" /> 
                </div>
                <div class="col-md-6 col-sm-6 col-print-6 col-md-offset-2 col-sm-offset-2 col-print-offset-2">
                    <div class="row">
                        <div class="col-md-2 col-sm-3 col-print-2">
                            <strong>Nama</strong>
                        </div>
                        <div class="col-md-10 col-sm-9 col-print-10">
                            <span>{{$archive->NAMA_KONSUMEN}}</span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2 col-sm-3 col-print-2">
                            <strong>Alamat</strong>
                        </div>
                        <div class="col-md-10 col-sm-9 col-print-10">
                            <span>@if($archive->ALAMAT_KONSUMEN){{$archive->ALAMAT_KONSUMEN}} @else - @endif</span>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2 col-sm-3 col-print-2">
                            <strong>HP/TLP</strong>
                        </div>
                        <div class="col-md-10 col-sm-9 col-print-10">
                            <span>@if($archive->ALAMAT_KONSUMEN){{$archive->PONSEL_KONSUMEN}} @else - @endif</span>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-10 col-sm-10 col-print-10 col-md-offset-1 col-sm-offset-1 col-print-offset-1">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Banyak</th>
                                <th>Status</th>
                                <th>Nama Barang</th>
                                <th>Harga</th>
                                <th>Sub Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products->list_product as $item)
                            <tr>
                                <td class="text-right">{{$item->QTY}}</td>
                                <td>{{$item->STATUS_PRODUCT}}</td>
                                <td>{{$item->NAMA_PRODUCT}}</td>
                                <td class="text-right">{{Helper::monetize($item->HARGA_PRODUCT)}}</td>
                                <td class="text-right">{{Helper::monetize($item->SUBTOTAL_PRODUCT)}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td class="text-right" colspan="4"><strong>TOTAL</strong></td>
                                <td class="text-right">{{Helper::monetize($products->total)}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="text-right">
                        <strong>STATUS: </strong>
                        <span class="@if($archive->STATUS == 'LUNAS') increase @else decrease @endif">
                            <strong>{{$archive->STATUS}}</strong>
                        </span>
                    </p>
                </div>
            </div>
            <hr />
            <div class="row text-right">
                <div class="col-md-5 col-sm-5 col-print-5 col-md-offset-7 col-sm-offset-7 col-print-offset-7">
                    <strong>Hormat Kami,</strong>
                    <br><br><br><br>
                    <strong>{{$archive->SELLER}}</strong>
                </div>
            </div>
        </div>
    </div>
</body>

<script src="{{asset('js/jquery-1.10.2.js')}}"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="{{asset('js/bootstrap.js')}}"></script>
<!-- METISMENU SCRIPTS -->
<script src="{{asset('js/jquery.metisMenu.js')}}"></script>
    <!-- CUSTOM SCRIPTS -->
<script src="{{asset('js/custom.js')}}"></script>

<script>
    $(document).ready(function(){
        window.print();
        setTimeout(window.close, 0);
    });
</script>