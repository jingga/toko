<!DOCTYPE html>
<html xmlns="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>@yield('title',"$toko->NAMA_TOKO")</title>
    <link rel="shortcut icon" href="{{$toko->LOGO_TOKO ? asset($toko->LOGO_TOKO) : asset('/images/no-thumbnail.png')}}" type="image/x-icon"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- BOOTSTRAP STYLES-->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet" />
    <!--CUSTOM BASIC STYLES-->
    <link href="{{asset('css/basic.css')}}" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="{{asset('css/custom.css')}}" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="{{asset('css/style.css')}}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="{{asset('css/material.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dataTables.material.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.flexdatalist.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-datepicker3.css')}}">

    @yield('head')
</head>
<body @if(!Session::has('user')) style="background-color: #E2E2E2;" @endif>
    @if(!Session::has('user'))
        @include('login')
    @else
        <div id="wrapper">
            <div class="loader hide" id="loader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{route('home')}}">{{$toko->NAMA_TOKO}}</a>
                </div>

                <div class="header-right">

                    <!-- <a href="#" class="btn btn-info" title="New Message"><b>30 </b><i class="fa fa-envelope-o fa-2x"></i></a>
                    <a href="#" class="btn btn-primary" title="New Task"><b>40 </b><i class="fa fa-bars fa-2x"></i></a> -->
                    <a href="{{route('logout')}}" class="btn btn-danger"><i class="fa fa-power-off"></i> Keluar</a>

                </div>
            </nav>
            <!-- /. NAV TOP  -->
            <nav class="navbar-default navbar-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="main-menu">
                        <li>
                            <div class="user-img-div"><div class="inner-text">
                                    {{$user->USERNAME}}
                                <br />
                                    <small>Last Login : {{$user->LAST_LOGIN ? date('Y-m-d H:i:s', strtotime($user->LAST_LOGIN)) : '-'}} </small>
                                </div>
                            </div>
                        </li>
                        @foreach($list_menu as $data_menu)
                        @continue(($data_menu->MENU_CODE == 'store' || $data_menu->MENU_CODE == 'employee' || $data_menu->MENU_CODE == 'sales') && $user->CREATED_BY != 'SYSTEM')
                        <li>
                            <a class="{{$data_menu->MENU_CODE}}" href="{{route($data_menu->MENU_CODE)}}"><i class="fa {{$data_menu->MENU_ICON}} "></i>{{$data_menu->MENU_DESCRIPTION}}</a>
                        </li>
                        @endforeach
                    </ul>

                </div>

            </nav>
            <!-- /. NAV SIDE  -->
            <div id="page-wrapper">
                <div id="page-inner">
                    <div class="hide" id="alert"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
                    @yield('content')
                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->

        <div class="modal fade" id="modal_warning" tabindex="-1" role="dialog" aria-labelledby="labelModal_warning" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header alert-warning">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="warning_title">Warning</h4>
                    </div>
                    <div class="modal-body">
                        <p id="warning_content">Apakah Anda yakin?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-danger" id="submit_warning">Yes</button>
                    </div>
                </div>
            </div>
        </div>

    <span id="curr_menu" data-value="{{$menu}}"></span>

    <div id="footer-sec">
        &copy; 2019 JinggaSoft | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
    </div>
    <!-- /. FOOTER  -->

    @endif

    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="{{asset('js/jquery-1.10.2.js')}}"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="{{asset('js/jquery.metisMenu.js')}}"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="{{asset('js/custom.js')}}"></script>

    <script src="{{asset('js/custom/general_function.js')}}"></script>
    <script src="{{asset('js/jquery.priceformat.min.js')}}"></script>

    <script type="text/javascript" charset="utf8" src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dataTables.material.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jquery.flexdatalist.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>

    @yield('bottom')
    
</body>
</html>
