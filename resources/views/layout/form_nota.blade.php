<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12" style="overflow-x:auto;">
        <table class="table table-responsive borderless modal-table-mobile">
            <thead>
                <th width="10%">Banyak</th>
                <th width="15%">Status</th>
                <th width="30%">Nama Barang</th>
                <th width="20%">Harga</th>
                <th width="20%">Jumlah</th>
                <th width="5%">&nbsp;</th>
            </thead>
            <tbody id="nota_description">
                <tr>
                    <td>
                        <div class="form-group">
                            <input class="form-control" type="text" name="item_qty[]" onkeypress="return typeNumber(event)">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <select class="form-control" type="text" name="item_status[]">
                                <option value="BELI">Beli</option>
                                <option value="RETUR">Retur</option>
                                <option value="BONUS">Bonus</option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" class="form-control flexdatalist" name="item_name[]" {{-- @if($menu=='invoice') data-selection-required="true" @endif --}} data-min-length='1' data-search-in='nama'>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input class="form-control" type="text" name="item_price[]" value="0">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input class="form-control" type="text" name="item_subtotal[]" value="0" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-right" colspan="5">
                        <button class="btn" type="button" id="add_row"><i class="glyphicon glyphicon-plus"></i> Tambah Barang</button>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td class="text-right vertical-middle" colspan="4">
                        <strong>Diskon</strong>
                    </td>
                    <td colspan="2">
                        <div class="form-group">
                            <input class="form-control" type="text" name="item_discount" value="0">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-right vertical-middle" colspan="4">
                        <strong>Total</strong>
                    </td>
                    <td colspan="2">
                        <div class="form-group">
                            <input class="form-control" type="text" name="item_total" value="0" readonly>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-right vertical-middle" colspan="4">
                        <strong>Pembayaran</strong>
                    </td>
                    <td colspan="2">
                        <div class="form-group">
                            <select class="form-control" type="text" name="status">
                                <option value="LUNAS">Lunas</option>
                                <option value="PENDING">Pending</option>
                            </select>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>