@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">SALES</h1>
            <div class="row page-subhead-line">
                <div class="col-md-2 col-md-offset-10">
                    <button class="btn btn-default" id="add_new_order" ><i class="glyphicon glyphicon-plus"></i> Tambah Order</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    List Order
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="order_table" class="table text-wrap mdl-data-table" style="width:100%">
                                <thead>
                                    <th>Nama Barang</th>
                                    <th>Sales</th>
                                    <th>Harga Barang</th>
                                    <th>Qty</th>
                                    <th>Pajak (%)</th>
                                    <th>Potongan (Rp)</th>
                                    <th>Total Harga</th>
                                    <th>Harga Ekspedisi (/pcs)</th>
                                    <th>Grand Total</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('bottom')
    <!-- <script src="{{asset('js/custom/employee_function.js')}}"></script> -->
@endsection