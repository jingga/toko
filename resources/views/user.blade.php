@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">USER</h1>
            @if($user->ROLE != 'MEMBER' || $user->CREATED_BY == 'SYSTEM')
            <div class="row page-subhead-line">
                <div class="col-sm-5 col-md-3 col-sm-offset-7 col-md-offset-9 text-right">
                    <button class="btn btn-default" id="add_new_user" ><i class="glyphicon glyphicon-plus"></i> Tambah User</button>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    List
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="user_table" class="table text-wrap mdl-data-table" style="width:100%">
                                <thead>
                                    <th>No.</th>
                                    <th>Username</th>
                                    <th>Status</th>
                                    <th>Role</th>
                                    <th>Menu</th>
                                    <th>Terakhir Login</th>
                                    <th>&nbsp;</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="submit_user">
        <div class="modal fade" id="modal_user" tabindex="-1" role="dialog" aria-labelledby="labelModal_user" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modal_title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id_user">
                        <div class="form-group">
                            <label>Username</label>
                            <input class="form-control" type="text" name="username">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input class="form-control" type="text" name="password">
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <select class="form-control" name="role">
                                <option value="ADMIN">Admin</option>
                                <option value="MEMBER">Member</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Menu</label>
                            @foreach($list_menu as $data)
                                @continue(stripos($data->MENU_CODE,'store') !== false || stripos($data->MENU_CODE,'employee') !== false)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="menu[]" value="{{$data->MENU_DESCRIPTION}}" id="menu_{{$data->MENU_CODE}}" />
                                    <label class="form-check-label" for="menu_{{$data->MENU_CODE}}">{{ucwords(strtolower($data->MENU_DESCRIPTION))}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="modal_submit" data-target="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <span data-url="{{route('getAllUser')}}" id="url_all_user"></span>
    <span data-url="{{route('insertUser')}}" id="insert_user"></span>
    <span data-url="{{route('updateUser')}}" id="update_user"></span>
    <span data-url="{{route('deleteUser')}}" id="delete_user"></span>
    
@endsection

@section('bottom')
    <script src="{{asset('js/custom/user_function.js')}}"></script>
@endsection