@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">ARSIP</h1>
            <div class="row page-subhead-line">
            {{-- @if($user->ROLE != 'MEMBER' || $user->CREATED_BY == 'SYSTEM')
                <div class="col-xs-6 col-sm-5 col-md-3 col-sm-offset-2 col-md-offset-6 ">
                    <button class="btn btn-default" id="add_new_archive" ><i class="glyphicon glyphicon-plus"></i> Tambah Arsip</button>
                </div>
            @endif --}}
                <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                    <button class="btn btn-default" id="create_invoice" ><i class="glyphicon glyphicon-plus"></i> Buka Nota</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    List
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="archive_table" class="table mdl-data-table" style="width:100%">
                                <thead>
                                    <th width="20%">Tanggal</th>
                                    <th width="25%">Nama</th>
                                    <th width="10%">Ponsel</th>
                                    <th width="25%">Alamat</th>
                                    <th width="10%">Status</th>
                                    <th width="10%">&nbsp;</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="form_nota">
        <input type="hidden" name="target_save">
        <div class="modal fade" id="modal_invoice" tabindex="-1" role="dialog" aria-labelledby="labelModal_invoice" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modal_title">Tambah Arsip</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <input class="form-control cursor_pointer" type="text" name="tanggal" placeholder="Klik Disini untuk merubah tanggal" readonly>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="nama_konsumen" placeholder="Nama">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="ponsel_konsumen" placeholder="Ponsel" onkeypress="return typeNumber(event)">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="alamat_konsumen" placeholder="Alamat"></textarea>
                                </div>
                            </div>
                        </div>
                        @include('layout.form_nota')
                    </div>
                    <div class="modal-footer" id="modal_footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="modal_submit" data-target="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <span data-url="{{route('save_invoice')}}" id="url_save_invoice"></span>
    <span data-url="{{route('print_invoice')}}" id="url_print_invoice"></span>
    <span data-url="{{route('getAllArchive')}}" id="url_all_archive"></span>
    <span data-url="{{route('updateStatusArchive')}}" id="url_update_archive"></span>
    <span data-url="{{route('getProductList')}}" id="url_all_product"></span>

@endsection

@section('bottom')
    <script src="{{asset('js/custom/archive_function.js')}}"></script>
@endsection