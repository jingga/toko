@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">NOTA</h1>
            <h1 class="page-subhead-line text-right">{{$tanggal}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form id="form_nota">
                <input type="hidden" name="target_save" value="new">
                <div class="row pad-top-botm">
                    <div class="col-lg-6 col-md-6 col-sm-6 ">
                        <img src="{{asset('images/logo/logo-invoice.png')}}" style="padding-bottom:20px;" /> 
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">           
                        <div class="form-group">
                            <input class="form-control" type="text" name="nama_konsumen" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="ponsel_konsumen" placeholder="Ponsel" onkeypress="return typeNumber(event)">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="alamat_konsumen" placeholder="Alamat"></textarea>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        @include('layout.form_nota')
                    </div>
                </div>
                <div class="row text-right">
                    <div class="col-md-1 col-md-offset-8">
                        <button class="btn" type="button" id="print_invoice">Cetak Nota</button>
                    </div>
                    <div class="col-md-1 col-md-offset-1">
                        <button class="btn" type="button" id="save_invoice">Tidak Cetak</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <span data-url="{{route('save_invoice')}}" id="url_save_invoice"></span>
    <span data-url="{{route('getProductList')}}" id="url_all_product"></span>
    
@endsection

@section('bottom')
    <script src="{{asset('js/custom/invoice_function.js')}}"></script>
@endsection