@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">{{$tittle}}</h1>
            @if($user->ROLE != 'MEMBER' || $user->CREATED_BY == 'SYSTEM')
            <div class="row page-subhead-line">
                <div class="col-sm-5 col-md-3 col-sm-offset-7 col-md-offset-9 text-right">
                    <button class="btn btn-default" id="add_new_income_outcome" ><i class="glyphicon glyphicon-plus"></i> Tambah {{ ucfirst(strtolower($tittle)) }}</button>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    List
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="income_outcome_table" class="table mdl-data-table" style="width:100%">
                                <thead>
                                    <th width="20%">Tanggal</th>
                                    <th width="60%">Keterangan</th>
                                    <th width="20%">Jumlah</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layout.modal_income_outcome')
    
    @if($menu == 'income')
        <span data-url="{{route('getIncomeList')}}" id="url_all_list"></span>
        <span data-url="{{route('save_income')}}" id="url_save_income_outcome"></span>
    @elseif($menu == 'outcome')
        <span data-url="{{route('getOutcomeList')}}" id="url_all_list"></span>
        <span data-url="{{route('save_outcome')}}" id="url_save_income_outcome"></span>
    @endif
@endsection

@section('bottom')
    <script src="{{asset('js/custom/income_function.js')}}"></script>
@endsection