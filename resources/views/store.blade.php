@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">TOKO</h1>
            <div class="row page-subhead-line">
                <div class="col-sm-5 col-md-3 col-sm-offset-7 col-md-offset-9 text-right">
                    <button class="btn btn-default" id="add_new_user" ><i class="glyphicon glyphicon-plus"></i> Tambah Toko</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    List
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="store_table" class="table text-wrap mdl-data-table" style="width:100%">
                                <thead>
                                    <th>No.</th>
                                    <th>Nama Toko</th>
                                    <th>URL Toko</th>
                                    <th width="15%">Logo</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="submit_store" enctype="multipart/form-data">
        <div class="modal fade" id="modal_store" tabindex="-1" role="dialog" aria-labelledby="labelModal_store" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modal_title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                            <input class="form-control" type="hidden" name="id_store">
                            <div class="form-group">
                                <label>Nama Toko</label>
                                <input class="form-control" type="text" name="nama_toko">
                            </div>
                            <div class="form-group">
                                <label>URL Toko</label>
                                <input class="form-control" type="text" name="url_toko">
                            </div>
                            <div class="form-group">
                                <label>Logo Toko</label>
                                <div class="file-upload">
                                    <button class="file-upload-btn" type="button">Add Logo</button>
                                    <div class="image-upload-wrap">
                                        <input class="form-control file-upload-input" type="file" name="logo_toko" accept="image/*" />
                                        <div class="drag-text">
                                            <h3>Drag and drop a file or click</h3>
                                        </div>
                                    </div>
                                    <div class="file-upload-content">
                                        <img class="file-upload-image" src="#" alt="your image" />
                                        <div class="image-title-wrap">
                                            <button type="button" class="remove-image">Remove</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select class="form-control" name="status">
                                    <option value="ACTIVE">Aktif</option>
                                    <option value="NONACTIVE">Non Aktif</option>
                                </select>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="modal_submit" data-target="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <span data-url="{{route('getAllStore')}}" id="url_all_store"></span>
    <span data-url="{{route('insertStore')}}" id="insert_store"></span>
    <span data-url="{{route('updateStore')}}" id="update_store"></span>
    <span data-url="{{route('deleteStore')}}" id="delete_store"></span>
    
@endsection

@section('bottom')
    <script src="{{asset('js/custom/store_function.js')}}"></script>
@endsection