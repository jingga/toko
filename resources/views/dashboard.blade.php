@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">DASHBOARD</h1>
            <h1 class="page-subhead-line text-right">Terakhir Update: {{$last_update}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading bg-black-font-white">
                    Finance
                </div>
                <div class="panel-body">
                    <div class="panel-group" id="summary">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#summary" href="#collapseToday" class="collapsed">{{date('l, d F Y')}} Summary <i class="glyphicon glyphicon-chevron-up"></i></a>
                                </h4>
                            </div>
                            <div id="collapseToday" class="panel-collapse in" style="height: auto;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="panel-danger simple-table">
                                                <div class="alert alert-danger text-center">
                                                    <h4>Pengeluaran</h4>
                                                    <h5 id="today_outcome"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-success simple-table">
                                                <div class="alert alert-success text-center">
                                                    <h4>Pemasukan</h4>
                                                    <h5 id="today_income"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-info simple-table">
                                                <div class="alert alert-info text-center">
                                                    <h4>Profit</h4>
                                                    <h5 id="today_profit"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-warning simple-table">
                                                <div class="alert alert-warning text-center">
                                                    <h4>Omset</h4>
                                                    <h5 id="today_turnover"></h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#summary" href="#month">{{date('F Y')}} Summary <i class="glyphicon glyphicon-chevron-down"></i></a>
                                </h4>
                            </div>
                            <div id="month" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="panel-danger simple-table">
                                                <div class="alert alert-danger text-center">
                                                    <h4>Pengeluaran</h4>
                                                    <h5 id="month_outcome"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-success simple-table">
                                                <div class="alert alert-success text-center">
                                                    <h4>Pemasukan</h4>
                                                    <h5 id="month_income"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-info simple-table">
                                                <div class="alert alert-info text-center">
                                                    <h4>Profit</h4>
                                                    <h5 id="month_profit"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-warning simple-table">
                                                <div class="alert alert-warning text-center">
                                                    <h4>Omset</h4>
                                                    <h5 id="month_turnover"></h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#summary" href="#year" class="collapsed">{{date('Y')}} Summary <i class="glyphicon glyphicon-chevron-down"></i></a>
                                </h4>
                            </div>
                            <div id="year" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="panel-danger simple-table">
                                                <div class="alert alert-danger text-center">
                                                    <h4>Pengeluaran</h4>
                                                    <h5 id="year_outcome"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-success simple-table">
                                                <div class="alert alert-success text-center">
                                                    <h4>Pemasukan</h4>
                                                    <h5 id="year_income"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-info simple-table">
                                                <div class="alert alert-info text-center">
                                                    <h4>Profit</h4>
                                                    <h5 id="year_profit"></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="panel-warning simple-table">
                                                <div class="alert alert-warning text-center">
                                                    <h4>Omset</h4>
                                                    <h5 id="year_turnover"></h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <span data-url="{{route('loadDashboard')}}" id="url_load_dashboard"></span>
    
@endsection

@section('bottom')
    <script src="{{asset('js/custom/dashboard_function.js')}}"></script>
@endsection