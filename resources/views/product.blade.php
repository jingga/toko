@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">PRODUK</h1>
            @if($user->ROLE != 'MEMBER' || $user->CREATED_BY == 'SYSTEM')
            <div class="row page-subhead-line">
                <div class="col-md-2 col-md-offset-10">
                    <button class="btn btn-default" id="add_new_product" ><i class="glyphicon glyphicon-plus"></i> Tambah Produk</button>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Product List
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="product_table" class="table text-wrap mdl-data-table" style="width:100%">
                                <thead>
                                    <th>No.</th>
                                    <th>Nama Produk</th>
                                    <th>Stok Produk</th>
                                    @if($user->ROLE != 'MEMBER' || $user->ROLE == 'SYSTEM')<th>Modal Produk</th> @endif
                                    <th>Harga Produk</th>
                                    <th>Status Produk</th>
                                    <th>Lokasi Produk</th>
                                    <th>&nbsp;</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row hide" id="history_section">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <span id="history_title"></span> History List <button class="close_table_detail close">&times;</button>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="history_table" class="table text-wrap mdl-data-table" style="width:100%">
                                <thead>
                                    <th width="15%">Tanggal</th>
                                    <th>Keterangan</th>
                                    <th width="5%">Stok Sebelum</th>
                                    <th width="5%">Jumlah</th>
                                    <th width="5%">Stok Sesudah</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    {{-- ADD EDIT PRODUCT --}}
    <form id="submit_product">
        <div class="modal fade" id="modal_product" tabindex="-1" role="dialog" aria-labelledby="labelModal_product" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modal_title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="id_product">
                        <div class="form-group">
                            <label>Nama Produk</label>
                            <input class="form-control" type="text" name="nama_product">
                        </div>
                        <div class="form-group">
                            <label>Stok</label>
                            <input class="form-control" type="text" name="stock_product">
                        </div>
                        <div class="form-group">
                            <label>Modal</label>
                            <input class="form-control" type="text" name="modal_product">
                        </div>
                        <div class="form-group">
                            <label>Harga</label>
                            <input class="form-control" type="text" name="harga_product">
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option value="AVAILABLE">Available</option>
                                <option value="NOT AVAILABLE">Not Available</option>
                                <option value="PENDING">Pending</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Lokasi</label>
                            <input class="form-control" type="text" name="lokasi_product">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="modal_submit" data-target="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    {{-- STOCK PRODUCT HISTORY --}}
    <form id="submit_history">
        <div class="modal fade" id="modal_history" tabindex="-1" role="dialog" aria-labelledby="labelModal_history" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modal_history_title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <input class="form-control" type="hidden" name="history_id_product">
                        <input class="form-control" type="hidden" name="history_action">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input class="form-control" type="text" name="description">
                        </div>
                        <div class="form-group">
                            <label>Qty</label>
                            <input class="form-control" type="text" name="qty" onkeypress="return typeNumber(event)">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="modal_history_submit">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    
    <span data-url="{{route('getAllProduct')}}" id="url_all_product"></span>
    <span data-url="{{route('insertProduct')}}" id="insert_product"></span>
    <span data-url="{{route('updateProduct')}}" id="update_product"></span>
    <span data-url="{{route('deleteProduct')}}" id="delete_product"></span>
    <span data-url="{{route('getHistoryProduct')}}" id="url_history"></span>
    <span data-url="{{route('updateStockProduct')}}" id="update_stock_product"></span>

    <span data-value="{{$user->ROLE}}" id="curr_role"></span>
@endsection

@section('bottom')
    <script src="{{asset('js/custom/product_function.js')}}"></script>
@endsection