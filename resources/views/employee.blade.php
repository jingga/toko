@extends('layout.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">KARYAWAN</h1>
            <div class="row page-subhead-line">
                <div class="col-md-2 col-md-offset-10">
                    <button class="btn btn-default" id="add_new_employee" ><i class="glyphicon glyphicon-plus"></i> Tambah Karyawan</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    List Karyawan
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="employee_table" class="table text-wrap mdl-data-table" style="width:100%">
                                <thead>
                                    <th>Nama</th>
                                    <th>Gaji</th>
                                    <th>Jenis Gaji</th>
                                    <th>Waktu Gajian</th>
                                    <th>Bulanan</th>
                                    <th>Bonus</th>
                                    <th>Status</th>
                                    <th>&nbsp;</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Absen Karyawan Hari Ini
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table id="absence_table" class="table text-wrap mdl-data-table" style="width:100%">
                                <thead>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th>Jumlah Gaji</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form id="submit_employee">
        <div class="modal fade" id="modal_employee" tabindex="-1" role="dialog" aria-labelledby="labelModal_employee" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="modal_title">Karyawan Baru</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" type="text" name="nama_karyawan">
                        </div>
                        <div class="form-group">
                            <label>Gaji</label>
                            <input class="form-control" type="text" name="salary">
                        </div>
                        <div class="form-group">
                            <label>Jenis Gaji</label>
                            <select class="form-control" name="salary_type">
                                <option value="DAILY">Harian</option>
                                <option value="MONTHLY">Bulanan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Waktu Gajian</label>
                            <select class="form-control" name="salary_due">
                                <option value="WEEKLY">Mingguan</option>
                                <option value="MONTHLY">Bulanan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Bulanan</label>
                            <input class="form-control" type="text" name="bonus_monthly">
                        </div>
                        <div class="form-group">
                            <label>Bonus</label>
                            <input class="form-control" type="text" name="bonus">
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control" name="status">
                                <option value="ACTIVE">Aktif</option>
                                <option value="NOT ACTIVE">Tidak Aktif</option>
                            </select>
                        </div>
                        <input class="form-control" type="hidden" name="id_karyawan">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="modal_submit" data-target="save">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <span data-url="{{route('getAllEmployee')}}" id="url_all_employee"></span>
    <span data-url="{{route('getEmployeeAbsence')}}" id="url_employee_absence"></span>
    <span data-url="{{route('insertEmployee')}}" id="url_insert_employee"></span>
    <span data-url="{{route('updateEmployee')}}" id="url_update_employee"></span>
    <span data-url="{{route('deleteEmployee')}}" id="url_delete_employee"></span>
    <span data-url="{{route('updateAbsence')}}" id="url_update_absence"></span>
    
@endsection
@section('bottom')
    <script src="{{asset('js/custom/employee_function.js')}}"></script>
@endsection