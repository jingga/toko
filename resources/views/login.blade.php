@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has($msg))
        <div class="alert alert-{{ $msg }}">{{ Session::get($msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
    @endif
@endforeach
<div class="container">
    <div class="row text-center " style="padding-top:100px;">
        <div class="col-md-12">
            <img src="{{$toko->LOGO_TOKO ? asset($toko->LOGO_TOKO) : asset('/images/no-thumbnail.png')}}" style="width:20%;"/>
        </div>
    </div>
    <div class="row ">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="panel-body">
                <form method="post" action="{{route('processLogin')}}">
                    {{ csrf_field() }}
                    <hr />
                    <h5>Please Login</h5>
                    <br />
                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                        <input type="text" class="form-control" name="username" placeholder="Your Username " />
                    </div>
                    <div class="form-group input-group">
                        <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                        <input type="password" class="form-control" name="password"  placeholder="Your Password" />
                    </div>
                    <button type="submit" class="btn btn-primary">Login Now</button>
                    <hr />
                </form>
            </div>
        </div>
    </div>
</div>