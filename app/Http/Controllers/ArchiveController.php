<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;

use App\Model\Archive as Archive;
use App\Model\Product as Product;
use App\Model\Finance as Finance;
use App\Model\Stock_History as History;
use App\Helpers\Helper;

use Session;
use DateTime;
use Carbon\Carbon;

class ArchiveController extends Controller
{
    public static function update_stock($new_archive, $list_product, $toko)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function update_stock');

        // update stock & income
        if($new_archive)
        {
            //finance variable
            $product_modal = 0;
            $harga_jual = 0;
            $total_income = 0;
            $total_turnover = 0;

            if($new_archive->STATUS != 'PENDING')
            {
                $check_today_income = Finance::where('TANGGAL','LIKE','%'.date('Y-m-d', strtotime($new_archive->TANGGAL)).'%')
                                    ->where('STATUS','PEMASUKAN')
                                    ->where('ID_TOKO', $toko->ID_TOKO)
                                    ->first();
            
                $income = [];
                $turnover = [];

                if(isset($check_today_income->ID_FINANCE))
                {
                    $total_income = $check_today_income->JUMLAH;
                    $income = [
                        'ID_TOKO' => $toko->ID_TOKO
                    ];
                }
                else
                {
                    $income = [
                        'ID_TOKO' => $toko->ID_TOKO,
                        'TANGGAL' => $new_archive->TANGGAL,
                        'KETERANGAN' => 'Nota Tanggal: ' . date('d-m-Y', strtotime($new_archive->TANGGAL)),
                        'STATUS' => 'PEMASUKAN'
                    ];
                }

                $check_today_turnover = Finance::where('TANGGAL','LIKE','%'.date('Y-m-d', strtotime($new_archive->TANGGAL)).'%')
                                        ->where('STATUS','OMSET')
                                        ->where('ID_TOKO', $toko->ID_TOKO)
                                        ->first();

                if(isset($check_today_turnover->ID_FINANCE))
                {
                    $total_turnover = $check_today_turnover->JUMLAH;
                    $turnover = [
                        'ID_TOKO' => $toko->ID_TOKO
                    ];
                }
                else
                {
                    $turnover = [
                        'ID_TOKO' => $toko->ID_TOKO,
                        'TANGGAL' => $new_archive->TANGGAL,
                        'KETERANGAN' => 'Nota Tanggal: ' . date('d-m-Y', strtotime($new_archive->TANGGAL)),
                        'STATUS' => 'OMSET'
                    ];
                }
            }
            
            foreach($list_product as $data)
            {
                $selected_product = Product::where('NAMA_PRODUCT',$data['NAMA_PRODUCT'])->where('ID_TOKO', $toko->ID_TOKO)->first();
                if(isset($selected_product->ID_PRODUCT))
                {
                    $sequence = History::where('ID_PRODUCT', $selected_product->ID_PRODUCT)->where('ID_TOKO', $toko->ID_TOKO)->count();
                    //stock history
                    $new_history = [
                        'ID_PRODUCT' => $selected_product->ID_PRODUCT,
                        'ID_TOKO' => $selected_product->ID_TOKO,
                        'TANGGAL' => date('Y-m-d'),
                        'DESCRIPTION' => $new_archive->NAMA_KONSUMEN . ' ' . ($new_archive->STATUS == 'PENDING' ? $new_archive->STATUS : $data['STATUS_PRODUCT']),
                        'STOCK_BEFORE' => $selected_product->STOCK_PRODUCT,
                        'QTY' => $data['STATUS_PRODUCT'] == 'RETUR' ? $data['QTY'] : -$data['QTY'],
                        'SEQUENCE' => ($sequence+1)
                    ];

                    $product_modal = ($selected_product->MODAL_PRODUCT) * $data['QTY'];
                    $harga_jual = $data['SUBTOTAL_PRODUCT'];

                    // update stock if status == beli || bonus
                    if($data['STATUS_PRODUCT'] != 'RETUR')
                    {
                        $selected_product->STOCK_PRODUCT -= $data['QTY'];
                        $total_income += ($harga_jual - $product_modal);
                        $total_turnover += $harga_jual;
                    }
                    else if($data['STATUS_PRODUCT'] == 'RETUR')
                    {
                        $selected_product->STOCK_PRODUCT += $data['QTY'];
                        $total_income -= ($product_modal - $harga_jual);
                        $total_turnover -= $harga_jual;
                    }

                    $new_history['STOCK_AFTER'] = $selected_product->STOCK_PRODUCT;
                    
                    History::create($new_history);

                    $selected_product->save();
                }
            }

            if($new_archive->STATUS != 'PENDING')
            {
                if(isset($new_archive['DISCOUNT']))
                {
                    $total_income -= $new_archive['DISCOUNT'];
                    $total_turnover -= $new_archive['DISCOUNT'];
                }
                
                //add update to income
                if(isset($check_today_income->ID_FINANCE))
                {
                    $income['JUMLAH'] = $total_income;
                    Finance::where('ID_FINANCE',$check_today_income->ID_FINANCE)->where('ID_TOKO', $toko->ID_TOKO)->update($income);
                }
                else
                {
                    $income['JUMLAH'] = $total_income;
                    Finance::create($income);
                }

                //add update to turnover
                if(isset($check_today_turnover->ID_FINANCE))
                {
                    $turnover['JUMLAH'] = $total_turnover;
                    Finance::where('ID_FINANCE',$check_today_turnover->ID_FINANCE)->where('ID_TOKO', $toko->ID_TOKO)->update($turnover);
                }
                else
                {
                    $turnover['JUMLAH'] = $total_turnover;
                    Finance::create($turnover);
                }
            }
        }
    }
    
    /* START NOTA */
    public function save_invoice(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function save_invoice');
        $data_request = $request->all();
        
        $status = new \stdClass();

        if($data_request && $request->exists('item_name'))
        {
            $list_product = [];
            $index = count($data_request['item_qty']);
            for($i = 0; $i<$index; $i++)
            {
                $selected_product = Product::where('NAMA_PRODUCT',$data_request['item_name'][$i])->where('ID_TOKO', $request->session()->get('toko')->ID_TOKO)->first();

                $list_product[] = [
                    'QTY' => $data_request['item_qty'][$i],
                    'NAMA_PRODUCT' => $data_request['item_name'][$i],
                    'STATUS_PRODUCT' => $data_request['item_status'][$i],
                    'HARGA_PRODUCT' => $data_request['item_price'][$i],
                    'SUBTOTAL_PRODUCT' => $data_request['item_subtotal'][$i],
                    'MODAL_PRODUCT' => $selected_product->MODAL_PRODUCT
                ];
            }

            $detail_product = [
                'list_product' => $list_product,
                'total' => $data_request['item_total']
            ];
            
            $new_invoice = [
                'TANGGAL' => $request->exists('tanggal') ? (new DateTime($data_request['tanggal']))->format('Y-m-d H:i:s') : new DateTime(),
                'NAMA_KONSUMEN' => $data_request['nama_konsumen'] ? strtoupper($data_request['nama_konsumen']) : 'CASH',
                'ALAMAT_KONSUMEN' => strtoupper($data_request['alamat_konsumen']),
                'PONSEL_KONSUMEN' => strtoupper($data_request['ponsel_konsumen']),
                'DETAIL_PRODUCT' => json_encode($detail_product),
                'DISCOUNT' => ($data_request['item_discount']) ? $data_request['item_discount'] : 0,
                'STATUS' => ($data_request['status']) ? $data_request['status'] : 'PENDING',
                'SELLER' => Session::get('user')['USERNAME']
            ];


            if($new_invoice && Session::has('toko'))
            {
                $getToko = Session::get('toko');
                $new_invoice['ID_TOKO'] = $getToko->ID_TOKO;
                $insert = Archive::create($new_invoice);
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Invoice');

                if($data_request['target_save'] == 'new')
                {
                    ArchiveController::update_stock($insert, $list_product, $getToko);
                }

                $status->code = 200;
                $status->message = 'Berhasil Membuat Nota Baru';
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on save_invoice Function');
                Log::info('Cause: Some Request are Empty. Given: ' .json_encode($new_invoice));
                
                $status->code = 500;
                $status->message = 'Terjadi Kesalahan saat Membuat Nota Baru';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on save_invoice Function');
            Log::info('Cause I: Some Request are Empty. Given: ' .json_encode($data_request));
            Log::info('Cause I: Item Name Not Exist. Given: ' .$request['item_name']);
            
            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Membuat Nota Baru';
        }

        
        return json_encode($status);
    }

    public function preview_invoice($id = null)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function preview_invoice');

        $toko =Session::get('toko');
        $status = new \stdClass();

        $decryptId = Crypt::decrypt($id);

        $selected_archived = Archive::where('ID_TOKO',$toko->ID_TOKO)
                            ->where('ID_ARCHIVE',$decryptId)
                            ->first();

        $products = json_decode($selected_archived->DETAIL_PRODUCT);

        return view('layout.print_nota')->with([
            'toko' => $toko,
            'archive' => $selected_archived,
            'products' => $products
        ]);

    }
    /* END NOTA */

    /* START ARCHIVE */
    public function get_all_archive(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_all_archive');

        $all_archive = [];
        if(Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $all_archive = Archive::where('ID_TOKO',$getToko->ID_TOKO)->get();
        }
        
        $archives=[];
        foreach($all_archive as $key => $archive)
        {
            $list_product = json_decode($archive->DETAIL_PRODUCT);
            $archives[] = [
                'tanggal' => date('d M Y', strtotime($archive->TANGGAL)),
                'nama' => ($archive->NAMA_KONSUMEN) ? $archive->NAMA_KONSUMEN : 'CASH',
                'alamat' => ($archive->ALAMAT_KONSUMEN) ? $archive->ALAMAT_KONSUMEN : '-',
                'ponsel' => ($archive->PONSEL_KONSUMEN) ? $archive->PONSEL_KONSUMEN : '-',
                'detail' => $list_product,
                'diskon' => Helper::monetize($archive->DISCOUNT),
                'total' => ($list_product->total) ? Helper::monetize($list_product->total) : Helper::monetize(0),
                'status' => $archive->STATUS,
                'id' => Crypt::encrypt($archive->ID_ARCHIVE),
                'seller' => ($archive->SELLER) ? $archive->SELLER : ''
            ];
        }
        $list = ["data" => $archives];
        
        return $list;
    }

    public function update_status_archive_to_lunas(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function update_status_archive_to_lunas');

        $data_request = $request->all();
        $status = new \stdClass();

        if($request->exists('id'))
        {
            $query = Archive::where('ID_ARCHIVE',$data_request['id']);
            $exist_archive = $query->first();

            //finance variable
            $product_modal = 0;
            $harga_jual = 0;
            $total_income = 0;
            $total_turnover = 0;

            if($exist_archive && $request->exists('status'))
            {
                $toko = $request->has('toko') ? $request->session()->get('toko') : Helper::checkSession_store();

                $exist_archive->STATUS = strtoupper($data_request['status']);
            
                $income = [];
                $turnover = [];

                $income = [
                    'ID_TOKO' => $toko->ID_TOKO,
                    'TANGGAL' => Carbon::now(),
                    'KETERANGAN' => 'Nota Pending: ' . date('d-m-Y', strtotime($exist_archive->TANGGAL)),
                    'STATUS' => 'PEMASUKAN'
                ];
                
                $turnover = [
                    'ID_TOKO' => $toko->ID_TOKO,
                    'TANGGAL' => Carbon::now(),
                    'KETERANGAN' => 'Nota Pending: ' . date('d-m-Y', strtotime($exist_archive->TANGGAL)),
                    'STATUS' => 'OMSET'
                ];

                $list_product = json_decode($exist_archive->DETAIL_PRODUCT);
                
                foreach($list_product->list_product as $data)
                {
                    $selected_product = Product::where('NAMA_PRODUCT',$data->NAMA_PRODUCT)->where('ID_TOKO', $toko->ID_TOKO)->first();

                    if($selected_product)
                    {
                        $qty = $data->STATUS_PRODUCT != 'RETUR' ? -$data->QTY : $data->QTY;

                        $history = History::where('ID_PRODUCT', $selected_product->ID_PRODUCT)
                                    ->where('ID_TOKO', $toko->ID_TOKO)
                                    ->where('TANGGAL','LIKE','%'.date('Y-m-d', strtotime($exist_archive->TANGGAL)).'%')
                                    ->where('DESCRIPTION','LIKE','%'. $exist_archive->NAMA_KONSUMEN . ' PENDING%')
                                    ->where('QTY', $qty)
                                    ->first();

                        if($history)
                        {
                            $history->DESCRIPTION = $exist_archive->NAMA_KONSUMEN . ' ' . $data->STATUS_PRODUCT;
                            $history->save();
                        }
                        
                        $product_modal = ($selected_product->MODAL_PRODUCT) * $data->QTY;
                        $harga_jual = $data->SUBTOTAL_PRODUCT;

                        if($data->STATUS_PRODUCT == 'BELI' || $data->STATUS_PRODUCT == 'BONUS')
                        {
                            $total_income += ($harga_jual - $product_modal);
                            $total_turnover += $harga_jual;
                        }
                        else if($data->STATUS_PRODUCT == 'RETUR')
                        {
                            $total_income -= ($product_modal - $harga_jual);
                            $total_turnover -= $harga_jual;
                        }

                        if($exist_archive->DISCOUNT)
                        {
                            $total_income -= $exist_archive->DISCOUNT;
                            $total_turnover -= $exist_archive->DISCOUNT;
                        }

                        $income['JUMLAH'] = $total_income;
                        Finance::create($income);

                        $turnover['JUMLAH'] = $total_turnover;
                        Finance::create($turnover);
                    }
                }
                
                $exist_archive->save();
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Update Status to "' . $request['status'] . '"');

                $status->code = 200;
                $status->message = 'Berhasil Memperbaharui Arsip';
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on update_status_archive_to_lunas Function');
                Log::info('Cause I: Archive Not Found. Given: ' .json_encode($exist_archive));
                Log::info('Cause II: Request "Status" Not Exist. Given: ' .$request['status']);
                
                $status->code = 500;
                $status->message = 'Terjadi Kesalahan saat Memperbaharui Arsip';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on update_status_archive_to_lunas Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($data_request));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Memperbaharui Arsip';
        }

        
        return json_encode($status);
    }
    /* END ARCHIVE */
}

?>