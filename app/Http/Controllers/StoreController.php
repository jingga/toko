<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Helpers\Helper as Helper;
use App\Model\Toko as Store;
use App\Model\Product;
use App\Model\Stock_History as History;
use App\Model\User;
use App\Model\Archive;
use App\Model\Finance as Finance;
use App\Model\Karyawan as Karyawan;
use App\Model\Absence_History as Absence;

use Session;
use DateTime;

class StoreController extends Controller
{
    private $tempPath = 'uploads/';
    private $acceptedExtension = Array('jpeg', 'jpg', 'png');

    public function check_request_has_file(Request $request, $all_request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function check_request_has_file');

        $maxSize = 5000000; //image size max = 5Mb
        $current_store = [];
        foreach($all_request as $key=>$data)
        {
            if($key == 'id_store' || $key == 'logo_toko')
                continue;

            if($key == 'url_toko')
                $current_store[strtoupper($key)] = $data;
            else
                $current_store[strtoupper($key)] = strtoupper($data);
        }

        $filename = preg_replace("/[^a-zA-Z0-9\s]/", "", $all_request['nama_toko']);
        $filename = str_ireplace(' ','-',$filename);

        $uploadPath = $this->tempPath . 'logo_store/';
        
        if(!file_exists($uploadPath))
        {
            Log::info(Session::get('user')['USERNAME'] . ' Create Directory for ' . $uploadPath);
            mkdir($uploadPath,0777,true);
        }

        if($request->hasFile('logo_toko'))
        {
            Log::info(Session::get('user')['USERNAME'] . ' Process Uploading Logo');

            $imgExt = $request->file('logo_toko')->getClientOriginalExtension();
            
            $imgSize = $request->file('logo_toko')->getSize();

            $filepath = '';
            if (in_array($imgExt, $this->acceptedExtension) && $imgSize <= $maxSize && $imgSize != "")
            {
                $newOriginalImageName  = strtolower($filename.'.'.$imgExt);

                $request->file('logo_toko')->move($uploadPath, $newOriginalImageName);

                $filepath = $uploadPath.$newOriginalImageName;

                $current_store['LOGO_TOKO'] = $filepath;

                Log::info(Session::get('user')['USERNAME'] . ' Finish Uploading Logo');

                return $current_store;
            }
            else
            {
                Log::info(Session::get('user')['USERNAME'] . ' Cancel Uploading Logo');
                Log::info('Cause I: Logo Extension. Accepted Extension: ' .$this->acceptedExtension. '. Given: ' .$imgExt);
                Log::info('Cause II: Logo Size. Max Size: ' .$maxSize. '. Given: ' .$imgSize);

                return false;
            }
        }
        else if($request['id_store'])
        {
            Log::info(Session::get('user')['USERNAME'] . ' Edit Store Information Without Changing Logo');

            return $current_store;
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' No Logo Image is Detected');
            return false;
        }
        
    }

    public function get_all_store()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_all_store');

        $all_store = Store::get();
        $stores = [];
        foreach($all_store as $store)
        {
            $stores[] = [
                'nama_toko' => $store->NAMA_TOKO,
                'url_toko' => $store->URL_TOKO,
                'logo' => $store->LOGO_TOKO,
                'status' => $store->STATUS,
                'id' => $store->ID_TOKO
            ];
        }
        array_multisort($stores, SORT_ASC);

        $list = ["data" => $stores];

        return $list;
    }

    public function insert_store(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function insert_store');

        $status = new \stdClass();
        $all_request = $request->all();

        if($all_request)
        {
            if($request->has('nama_toko'))
            {
                Log::info(Session::get('user')['USERNAME'] . ' Process Insert Store');
                $new_store = StoreController::check_request_has_file($request, $all_request);
                
                if(is_array($new_store))
                {
                    Store::create($new_store);

                    Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Store');
                    $status->code = 200;
                    $status->message = 'Berhasil Menambahkan Toko Baru';
                }
                else if(!$request->hasFile('logo_toko'))
                {
                    Log::error(Session::get('user')['USERNAME'] . ' Request contains No Logo');
                    $status->code = 500;
                    $status->message = 'File Logo tidak ditemukan';
                }
                else
                {
                    Log::error(Session::get('user')['USERNAME'] . ' Invalid Request Logo Extension');
                    $status->code = 500;
                    $status->message = 'Format File harus .jpg/.jpeg/.png';
                }
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on insert_store Function');
                Log::info('Cause: Store Name is Empty. Given: ' .json_encode($all_request));

                $status->code = 500;
                $status->message = 'Terjadi Kesalahan saat Menambahkan Toko Baru';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on insert_store Function');
            Log::info('Cause: Request is Empty. Given: ' .json_encode($all_request));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menambahkan Toko Baru';
        }
        
        return json_encode($status);
    }

    public function update_store(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function update_store');

        $status = new \stdClass();
        $all_request = $request->all();

        if($request->exists('id_store'))
        {
            $query = Store::where('ID_TOKO',$request['id_store']);
            $exist_store = $query->first();

            if($exist_store)
            {
                Log::info(Session::get('user')['USERNAME'] . ' Process Update [' .$exist_store['NAMA_TOKO']. ']\'s Information');

                $update_store = StoreController::check_request_has_file($request, $all_request);
                
                if(is_array($update_store))
                {
                    $query->update($update_store);
                    Log::info(Session::get('user')['USERNAME'] . ' Successfully Update [' .$exist_store['NAMA_TOKO']. ']\'s Information');
                    $status->code = 200;
                    $status->message = 'Berhasil Memperbaharui Toko';
                }
                else
                {
                    Log::error(Session::get('user')['USERNAME'] . ' Error on update_store Function');
                    Log::info('Cause: Request is Empty. Given: ' .json_encode($all_request));
                    $status->code = 500;
                    $status->message = 'Terjadi Kesalahan saat Memperbaharui Toko';
                }
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' No Store Found. ID: ' .$request['id_store']);
                $status->code = 500;
                $status->message = 'Toko Tidak Ditemukan';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on update_store Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($all_request));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Memperbaharui Toko';
        }

        return json_encode($status);
    }

    public function delete_store(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function delete_store');

        $status = new \stdClass();

        if($request->exists('id_store'))
        {
            $query = Store::where('ID_TOKO',$request['id_store']);
            $exist_store = $query->first();

            if($exist_store)
            {
                Log::info(Session::get('user')['USERNAME'] . ' Process Delete Store. ID: ' .$exist_store->ID_TOKO);
                if(file_exists($exist_store->LOGO_TOKO))
                {
                    Log::info(Session::get('user')['USERNAME'] . ' Delete Store Logo File: ' .$exist_store->LOGO_TOKO);
                    unlink($exist_store->LOGO_TOKO);
                }

                Archive::where('ID_TOKO',$request['id_store'])->delete();
                Karyawan::where('ID_TOKO',$request['id_store'])->delete();
                Absence::where('ID_TOKO',$request['id_store'])->delete();
                Finance::where('ID_TOKO',$request['id_store'])->delete();
                Product::where('ID_TOKO',$request['id_store'])->delete();
                History::where('ID_TOKO',$request['id_store'])->delete();
                User::where('ID_TOKO',$request['id_store'])->delete();

                $query->delete();
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Delete Store');

                $status->code = 200;
                $status->message = 'Berhasil Menghapus Toko';
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' No Store Found. ID: ' .$request['id_store']);
                $status->code = 500;
                $status->message = 'Toko Tidak Ditemukan';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on delete_store Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($all_request));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menghapus Toko';
        }
        
        return json_encode($status);
    }
}
?>