<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

use App\Model\Karyawan as Karyawan;
use App\Model\Absence_History as Absence;
use App\Helpers\Helper;

use Session;

class EmployeeController extends Controller
{
    public function get_all_employee()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_all_employee');

        $all_employee = [];
        if(Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $all_employee = Karyawan::where('ID_TOKO',$getToko->ID_TOKO)->get();
        }

        $employees = [];
        foreach($all_employee as $key => $employee)
        {
            $employees[] = [
                'nama' => $employee->NAMA_KARYAWAN,
                'salary' => Helper::monetize($employee->SALARY),
                'jenis_gaji' => $employee->SALARY_TYPE,
                'waktu_bayar' => $employee->SALARY_DUE,
                'bonus' => Helper::monetize($employee->BONUS),
                'bulanan' => Helper::monetize($employee->BONUS_MONTHLY),
                'status' => $employee->STATUS,
                'id' => $employee->ID_KARYAWAN
            ];
        }
        array_multisort($employees, SORT_ASC);

        $list = ["data" => $employees];

        return $list;
    }

    public function insert_employee(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function insert_employee');

        $data_request = $request->all();

        if($data_request && $request->exists('id_karyawan'))
        {
            $new_employee = [];
            $status = new \stdClass();
            foreach($data_request as $key=>$data)
            {
                if($key == 'id_karyawan')
                    continue;

                $new_employee[strtoupper($key)] = strtoupper($data);
            }

            if($new_employee && Session::has('toko'))
            {
                $getToko = Session::get('toko');
                $new_employee['ID_TOKO'] = $getToko->ID_TOKO;
                $insert = Karyawan::create($new_employee);

                Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert New Employee: ' .$new_employee['NAMA_KARYAWAN']);

                $status->code = 200;
                $status->message = 'Berhasil Tambahkan Karyawan Baru';
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on insert_employee Function');
                Log::info('Cause: Some Request are Empty. Given: ' .json_encode($new_employee));
                
                $status->code = 500;
                $status->message = 'Terjadi Kesalahan saat Menambahkan Karyawan Baru';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on insert_employee Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($data_request));
            
            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menambahkan Karyawan Baru';
        }

        
        return json_encode($status);
    }

    public function update_employee(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function update_employee');

        $data_request = $request->all();

        $update_employee = [];
        $status = new \stdClass();

        if($request['id_karyawan'])
        {
            $query = Karyawan::where('ID_KARYAWAN', $request['id_karyawan']);
            $exists_employee = $query->first();

            if($exists_employee)
            {
                foreach($data_request as $key=>$data)
                {
                    if($key == 'id_karyawan')
                        continue;

                    $update_employee[strtoupper($key)] = strtoupper($data);
                }

                if($update_employee)
                {
                    $query->update($update_employee);

                    Log::info(Session::get('user')['USERNAME'] . ' Successfully Update Employee: ' .$exists_employee['NAMA_KARYAWAN']);

                    $status->code = 200;
                    $status->message = 'Berhasil Memperbaharui Karyawan';
                }
                else
                {
                    Log::error(Session::get('user')['USERNAME'] . ' Error on update_employee Function');
                    Log::info('Cause: Some Request are Empty. Given: ' .json_encode($update_employee));
                    
                    $status->code = 500;
                    $status->message = 'Terjadi Kesalahan saat Memperbaharui Karyawan';
                }
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on update_employee Function');
                Log::info('Cause: Employee Not Found.');
                
                $status->code = 500;
                $status->message = 'Karyawan Tidak Ditemukan';
            }

        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on update_employee Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($data_request));
            
            $status->code = 500;
            $status->message = 'Terjadi Kesalahan Pada Server';
        }
        return json_encode($status);
    }

    public function delete_employee(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function delete_employee');

        $status = new \stdClass();

        if($request['id_karyawan'])
        {
            $query = Karyawan::where('ID_KARYAWAN',$request['id_karyawan']);
            $exist_employee = $query->first();

            if($exist_employee)
            {
                $the_absence = Absence::where('ID_KARYAWAN', $request['id_karyawan'])->delete();
                $query->delete();

                Log::info(Session::get('user')['USERNAME'] . ' Successfully Delete Employee');

                $status->code = 200;
                $status->message = 'Berhasil Menghapus Karyawan';
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on delete_employee Function');
                Log::info('Cause: Employee Not Found.');

                $status->code = 500;
                $status->message = 'Karyawan Tidak Ditemukan';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on insert_employee Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($request->all()));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan Pada Server';
        }

        return json_encode($status);
    }

    public function get_employee_absence(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_employee_absence');
        
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);

        $all_employee = [];
        if(Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $all_employee = Karyawan::where('ID_TOKO',$getToko->ID_TOKO)->get();
        }

        $absences = [];
        foreach($all_employee as $key => $employee)
        {
            $query = '';
            $total_masuk = 0;
            $total_pay = 0;

            if($employee->SALARY_DUE = 'WEEKLY' && $employee->SALARY_TYPE = 'DAILY')
            {
                $query = Absence::where('ID_KARYAWAN',$employee->ID_KARYAWAN)
                    ->where('STATUS','MASUK')
                    ->whereBetween('TANGGAL',[Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
                
                $total_masuk = $query->count();
            }
            else if($employee->SALARY_DUE == 'MONTHLY')
            {
                if($employee->SALARY_TYPE = 'DAILY')
                {
                    $query = Absence::where('ID_KARYAWAN',$employee->ID_KARYAWAN)
                    ->where('STATUS','MASUK')
                    ->whereMonth('TANGGAL',date('m'));

                    $total_masuk = $query->count();
                }
                else
                    $total_masuk = 1;
            }

            $total_pay = $total_masuk * $employee->SALARY;

            $today_absence = Absence::where('ID_KARYAWAN',$employee->ID_KARYAWAN)
                            ->where('STATUS','MASUK')
                            ->where('TANGGAL','LIKE','%'.date('Y-m-d').'%')->count();

            $absences[] = [
                'nama' => $employee->NAMA_KARYAWAN,
                'salary' => Helper::monetize($total_pay),
                'status' => $today_absence ? 'MASUK' : 'ABSEN',
                'id' => $employee->ID_KARYAWAN
            ];
        }
        array_multisort($absences, SORT_ASC);

        $list = ["data" => $absences];

        return $list;
    }

    public function update_absence(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function update_absence');

        $status = new \stdClass();
        if(isset($request['id']) && isset($request['status']))
        {
            $history_exists = Absence::where('ID_KARYAWAN',$request['id'])->where('TANGGAL','LIKE','%'.date('Y-m-d').'%')->first();

            if(isset($history_exists->ID_ABSENCE))
            {
                $history_exists->STATUS = strtoupper($request['status']);
                $history_exists->save();

                Log::info(Session::get('user')['USERNAME'] . ' Successfully Update Absence to ' .$history_exists->STATUS);
            }
            else if(Session::has('toko'))
            {
                $getToko = Session::get('toko');

                $new_absence = [
                    'ID_KARYAWAN' => $request['id'],
                    'ID_TOKO' => $getToko->ID_TOKO,
                    'TANGGAL' => date('Y-m-d'),
                    'STATUS' => $request['status']
                ];

                $insert_history = Absence::create($new_absence);

                Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert New Absence History for New Employee');
            }
        }
    }
}
?>