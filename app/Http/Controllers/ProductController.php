<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Model\Product as Product;
use App\Model\Stock_History as History;
use App\Model\Finance as Finance;

use App\Helpers\Helper;

use Session;
use Carbon\Carbon;

class ProductController extends Controller
{
    public static function call_product_list_from_database()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function call_product_list_from_database');

        $current_role = Session::get('user')['ROLE'];

        $all_product=[];
        if(Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $all_product = Product::where('ID_TOKO',$getToko->ID_TOKO)->get();
        }
        
        $products=[];
        foreach($all_product as $key => $product)
        {
            if($current_role != 'MEMBER')
            {
                $products[] = [
                    'nama' => $product->NAMA_PRODUCT,
                    'stok' => $product->STOCK_PRODUCT,
                    'modal' => Helper::monetize($product->MODAL_PRODUCT),
                    'harga' => Helper::monetize($product->HARGA_PRODUCT),
                    'lokasi' => $product->LOKASI_PRODUCT,
                    'status' => $product->STATUS,
                    'id' => $product->ID_PRODUCT
                ];
            }
            else
            {
                $products[] = [
                    'nama' => $product->NAMA_PRODUCT,
                    'stok' => $product->STOCK_PRODUCT,
                    'harga' => Helper::monetize($product->HARGA_PRODUCT),
                    'lokasi' => $product->LOKASI_PRODUCT,
                    'status' => $product->STATUS,
                    'id' => $product->ID_PRODUCT
                ];
            }
        }
        array_multisort($products, SORT_ASC);

        return $products;
    }

    public function get_all_product()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_all_product');
        $products = ProductController::call_product_list_from_database();
        $list = ["data" => $products];
        
        return $list;
    }

    public function load_all_product()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function load_all_product');
        $products = ProductController::call_product_list_from_database();

        return $products;
    }

    public function insert_product_to_db(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function insert_product_to_db');

        $data_request = $request->all();
        
        $new_product = [];
        $status = new \stdClass();

        foreach($data_request as $key=>$data)
        {
            if($key == 'id_product')
                continue;

            $new_product[strtoupper($key)] = strtoupper($data);
        }

        if($new_product && Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $new_product['ID_TOKO'] = $getToko->ID_TOKO;
            $insert = Product::create($new_product);
            Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Product');

            if(isset($insert->ID_PRODUCT))
            {
                $sequence = History::where('ID_PRODUCT', $insert->ID_PRODUCT)->where('ID_TOKO', $getToko->ID_TOKO)->count();
                $product_history = [
                    'ID_TOKO' => $insert->ID_TOKO,
                    'ID_PRODUCT' => $insert->ID_PRODUCT,
                    'TANGGAL' => date('Y-m-d'),
                    'DESCRIPTION' => 'PRODUK BARU',
                    'QTY' => (int) $insert->STOCK_PRODUCT,
                    'STOCK_BEFORE' => 0,
                    'STOCK_AFTER' => (int) $insert->STOCK_PRODUCT,
                    'SEQUENCE' => ($sequence+1)
                ];

                $insert_history = History::create($product_history);
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Product History');

            }
            $status->code = 200;
            $status->message = 'Berhasil Tambahkan Produk Baru';
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on insert_product_to_db Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($data_request));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menambahkan Produk Baru';
        }

        return json_encode($status);
    }

    public function update_product_to_db(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function update_product_to_db');

        $data_request = $request->all();
        $update_product = [];
        $status = new \stdClass();

        if($request->exists('id_product'))
        {
            $query = Product::where('ID_PRODUCT',$request['id_product']);
            $exist_product = $query->first();

            if($exist_product)
            {
                $current_role = Session::get('user')['ROLE'];
                foreach($data_request as $key=>$data)
                {
                    if($key == 'id_product')
                        continue;
                    if($current_role == 'MEMBER' && $key == 'modal_product')
                        continue;

                    $update_product[strtoupper($key)] = strtoupper($data);
                }

                if($update_product)
                {
                    if($exist_product->STOCK_PRODUCT != $request['stock_product'])
                    {
                        $sequence = History::where('ID_PRODUCT', $exist_product->ID_PRODUCT)->where('ID_TOKO', $exist_product->ID_TOKO)->count();
                        $product_history = [
                            'ID_TOKO' => $exist_product->ID_TOKO,
                            'ID_PRODUCT' => $exist_product->ID_PRODUCT,
                            'TANGGAL' => date('Y-m-d'),
                            'DESCRIPTION' => 'EDIT PRODUK',
                            'QTY' => abs(((int) $request['stock_product']) - $exist_product->STOCK_PRODUCT),
                            'STOCK_BEFORE' => $exist_product->STOCK_PRODUCT,
                            'STOCK_AFTER' => (int) $request['stock_product'],
                            'SEQUENCE' => ($sequence+1)
                        ];

                        $insert_history = History::create($product_history);
                        Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Product History by Changing The Stock');
                    }

                    $query->update($update_product);

                    Log::info(Session::get('user')['USERNAME'] . ' Successfully Update Product Information');
                    $status->code = 200;
                    $status->message = 'Berhasil Memperbaharui Produk';
                }
                else
                {
                    Log::error(Session::get('user')['USERNAME'] . ' Error on update_product_to_db Function');
                    Log::info('Cause: Some Request are Empty. Given: ' .json_encode($update_product));

                    $status->code = 500;
                    $status->message = 'Terjadi Kesalahan saat Memperbaharui Produk';
                }
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on update_product_to_db Function');
                Log::info('Cause: Product Not Found');

                $status->code = 500;
                $status->message = 'Produk Tidak Ditemukan';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on update_product_to_db Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($data_request));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Memperbaharui Produk';
        }
        
        return json_encode($status);
    }

    public function delete_product_from_db(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function delete_product_from_db');
        $status = new \stdClass();

        if($request->exists('id_product'))
        {
            $query = Product::where('ID_PRODUCT',$request['id_product']);
            $exist_product = $query->first();

            if($exist_product)
            {
                $the_history = History::where('ID_PRODUCT', $request['id_product'])->delete();
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Delete History Product: ' .$exist_product['NAMA_PRODUCT']);

                $query->delete();
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Delete Product Information');
                $status->code = 200;
                $status->message = 'Berhasil Menghapus Produk';
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on delete_product_from_db Function');
                Log::info('Cause: Product not Found');
                $status->code = 500;
                $status->message = 'Produk Tidak Ditemukan';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on delete_product_from_db Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($request->all()));
            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menghapus Produk';
        }
        
        return json_encode($status);
    }

    public function get_list_history_of_product(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_list_history_of_product');
        $history_list = [];

        if(isset($request['id_product']))
        {
            $getHistory = History::where('ID_PRODUCT', $request['id_product'])->orderBy('TANGGAL','DESC')->get();

            foreach($getHistory as $history)
            {
                $history_list[] = [
                    'order' => $history->SEQUENCE,
                    'tanggal' => date('d-m-Y', strtotime($history->TANGGAL)),
                    'keterangan' => $history->DESCRIPTION,
                    'before' => $history->STOCK_BEFORE,
                    'qty' => $history->QTY,
                    'after' => $history->STOCK_AFTER,
                ];
            }

            array_multisort($history_list, SORT_ASC);
        }

        
        return ["data" => $history_list];
        
    }

    public function update_stock_product_history(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function update_stock_product_history');
        $status = new \stdClass();

        if($request->exists('history_id_product'))
        {
            $query_product = Product::where('ID_PRODUCT', $request['history_id_product']);
        
            $selected_product = $query_product->first();

            if(isset($selected_product->ID_PRODUCT))
            {
                $qty = (int) $request['qty'];
                $qty = ($request['history_action'] == 'in') ?  $qty: -$qty;
                $stock_after = $selected_product->STOCK_PRODUCT + $qty;

                $sequence = History::where('ID_PRODUCT', $selected_product->ID_PRODUCT)->count();

                $new_history = [
                    'ID_PRODUCT' => $selected_product->ID_PRODUCT,
                    'ID_TOKO' => $selected_product->ID_TOKO,
                    'TANGGAL' => Carbon::now(),
                    'DESCRIPTION' => $request['description'],
                    'QTY' => $qty,
                    'STOCK_BEFORE' => $selected_product->STOCK_PRODUCT,
                    'STOCK_AFTER' => $stock_after,
                    'SEQUENCE' => ($sequence+1)
                ];

                $insert_history = History::create($new_history);
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Product History Triggered By Action \'' .$request['history_action']. '\'');

                $selected_product->STOCK_PRODUCT = $stock_after;
                $selected_product->save();
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Update Stock Product History');

                if($request['history_action'] == 'out')
                {
                    $outcome = [
                        'TANGGAL' => Carbon::now(),
                        'KETERANGAN' => $selected_product->NAMA_PRODUCT . ' ' . $request['description'] . ' ' . $qty,
                        'STATUS' => 'PENGELUARAN',
                        'JUMLAH' => ($request['qty'] * $selected_product->MODAL_PRODUCT),
                        'ID_TOKO' => $selected_product->ID_TOKO
                    ];

                    Finance::create($outcome);
                    Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Outcome Triggered By Action \'' .$request['history_action']. '\'');
                    Log::info('Cause: ' . $outcome['KETERANGAN']);
                }
                
                $status->code = 200;
                $status->message = 'Berhasil Memperbaharui Stok Produk ' . $selected_product->NAMA_PRODUCT;
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on update_stock_product_history Function');
                Log::info('Cause: Product Not Found');
                $status->code = 500;
                $status->message = 'Terjadi Kesalahan pada Server saat Memperbaharui Stok Produk';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on update_stock_product_history Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($request->all()));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan pada Server saat Memperbaharui Stok Produk';
        }
        
        return json_encode($status);
        
    }
}

?>