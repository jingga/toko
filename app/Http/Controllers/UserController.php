<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

use App\Model\User as User;
use App\Helpers\Helper as Helper;

use Session;
use DateTime;

class UserController extends Controller
{
    public function service_register_user(Request $request)
    {
        $status = new \stdClass();
        $all_request = $request->all();

        $toko = Helper::checkSession_store();
        $log_trigger = '';

        if($request->session()->has('user'))
            $log_trigger = $request->session()->get('user')['USERNAME'];
        else
            $log_trigger = $toko['NAMA_TOKO'];
        
        Log::info($log_trigger . ' Call Function service_register_user');

        if($all_request)
        {
            if($request->has('username'))
            {
                $checkUser = User::where('USERNAME',strtoupper($request['username']))->where('ID_TOKO', $toko->ID_TOKO)->first();
                if(isset($checkUser->ID_USER))
                {
                    Log::error($log_trigger . ' Error on Register User');
                    Log::info('Cause: User Already Exist');

                    $status->code = 500;
                    $status->message = 'User Sudah Ada';
                    return json_encode($status);
                }
            }

            $register_user = [];
            foreach($all_request as $key=>$data)
            {
                if(($key == 'created_by' || $key == 'updated_by') && $data == 'system')
                    $register_user[strtoupper($key)] = strtoupper($data);
                else if($key == 'password')
                    $register_user[strtoupper($key)] = Crypt::encrypt($data);
                else if($key == 'menu')
                    $register_user[strtoupper($key)] = json_encode($data);
                else
                    $register_user[strtoupper($key)] = strtoupper($data);
            }

            if(!array_key_exists('CREATED_BY',$register_user) || !array_key_exists('UPDATED_BY',$register_user))
            {
                if(Session::has('user'))
                {
                    $currentUser = Session::get('user');
                    $register_user['CREATED_BY'] = $currentUser->ID_USER;
                    $register_user['UPDATED_BY'] = $currentUser->ID_USER;
                }
            }

            if($register_user)
            {
                $register_user['ID_TOKO'] = $toko->ID_TOKO;
                $register_user['STATUS'] = 'ACTIVE';
            }
            else
            {
                Log::error($log_trigger . ' Error on Register User');
                Log::info('Cause: Some Request are Empty. Given: ' .json_encode($register_user));
                $status->code = 500;
                $status->message = 'Terjadi Kesalahan saat Menambahkan User Baru';
                return json_encode($status);
            }

            User::create($register_user);
            $status->code = 200;
            $status->message = 'Berhasil Menambahkan User Baru';

            Log::info($log_trigger . ' Successfully Create New User. Username: ' . $register_user['USERNAME']);
        }
        else
        {
            Log::error($log_trigger . ' Error on Register User');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($all_request));
            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menambahkan User Baru';
        }
        

        return json_encode($status);
    }

    public function get_all_user()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_all_user');

        $currentUser = Session::get('user');
        $all_user = [];
        if(Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $query = User::where('ID_TOKO',$getToko->ID_TOKO)->where('ID_USER','!=',$currentUser->ID_USER);

            if($currentUser->CREATED_BY != 'SYSTEM')
                $query->where('CREATED_BY','!=','SYSTEM');

            $all_user = $query->get();
        }

        $users = [];
        foreach($all_user as $user)
        {
            $users[] = [
                'username' => $user->USERNAME,
                'role' => $user->ROLE,
                'menu' => $user->MENU,
                'status' => $user->STATUS,
                'last_login' => $user->LAST_LOGIN,
                'id' => $user->ID_USER
            ];
        }
        array_multisort($users, SORT_ASC);

        $list = ["data" => $users];

        return $list;
    }

    public function insert_user_to_db(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function insert_user_to_db');

        $status = UserController::service_register_user($request);

        return $status;
    }

    public function update_user_to_db(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function update_user_to_db');

        $status = new \stdClass();
        $all_request = $request->all();
        $update_user = [];

        if($request->exists('id_user'))
        {
            $query = User::where('ID_USER',$request['id_user']);
            $exist_user = $query->first();

            if($exist_user)
            {
                foreach($all_request as $key=>$data)
                {
                    if($key == 'id_user' || $key == 'password')
                        continue;

                    if($key == 'menu')
                        $update_user[strtoupper($key)] = json_encode($data);
                    else
                        $update_user[strtoupper($key)] = strtoupper($data);
                }

                if($update_user)
                {
                    $currentUser = Session::get('user');
                    $update_user['UPDATED_BY'] = $currentUser->ID_USER;
                    
                    $query->update($update_user);
                    $status->code = 200;
                    $status->message = 'Berhasil Memperbaharui User';
                    Log::info(Session::get('user')['USERNAME'] . ' Successfully Update '.$exist_user['USERNAME'].'\'s Information');
                }
                else
                {
                    Log::error(Session::get('user')['USERNAME'] . ' Error on update_user_to_db Function');
                    Log::info('Cause: Some Request are Empty. Given: ' .json_encode($update_user));

                    $status->code = 500;
                    $status->message = 'Terjadi Kesalahan saat Memperbaharui User';
                }
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on update_user_to_db Function');
                Log::info('Cause: User Not Exist. Username: ' .$request['id_user']);

                $status->code = 500;
                $status->message = 'User Tidak Ditemukan';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on update_user_to_db Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($all_request));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Memperbaharui User';
        }

        return json_encode($status);
    }

    public function delete_user_from_db(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function delete_user_drom_db');

        $status = new \stdClass();

        if($request->exists('id_user'))
        {
            $query = User::where('ID_USER',$request['id_user']);
            $exist_user = $query->first();

            if($exist_user)
            {
                $query->delete();
                $status->code = 200;
                $status->message = 'Berhasil Menghapus User';
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Delete User: ' . $exist_user['USERNAME']);
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error on delete_user_from_db Function');
                Log::info('Cause: User ' . $request['id_user'] . ' Not Found.');

                $status->code = 500;
                $status->message = 'User Tidak Ditemukan';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error on delete_user_from_db Function');
            Log::info('Cause: Some Request are Empty. Given: ' .json_encode($request->all()));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menghapus User';
        }
        
        return json_encode($status);
    }
}

?>