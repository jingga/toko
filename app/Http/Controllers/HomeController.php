<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

use App\Helpers\Helper;
use App\Model\Product;
use App\Model\User;
use App\Model\Toko;
use App\Model\Archive;

use DateTime;
use Session;
use Carbon\Carbon;

class HomeController extends Controller
{
    private $toko = [];
    private $menu = [];

    public function __construct()
    {
        $this->toko = Helper::checkSession_store();
        $this->menu = Helper::get_menu();
    }

    public function login_page()
    {
        $this->toko = Helper::checkSession_store();
        Log::info($this->toko['NAMA_TOKO'] . ' Initiate Login Page');

        return view('layout.app')->with([
            'toko' => $this->toko
        ]);
    }

    public function login_process(Request $request)
    {
        Log::info($this->toko['NAMA_TOKO'] . ' Login Process');
        $all_request = $request->all();
        $this->toko = Helper::checkSession_store();

        if($all_request)
        {
            Log::info($this->toko['NAMA_TOKO'] . ' ' . $request['username'] . ' Try to Log In');
            $checkUser = User::where('USERNAME',$request['username'])->where('ID_TOKO',$this->toko->ID_TOKO)->first();

            if(!isset($checkUser->USERNAME))
            {
                Log::error($this->toko['NAMA_TOKO'] . ' No User Found');
                $request->session()->flash('danger', 'Username atau Passsword Anda Salah');
                return redirect()->route('login');
            }
            
            $userPassword = Crypt::decrypt($checkUser->PASSWORD);
            if($request['password'] == $userPassword)
            {
                Log::info($this->toko['NAMA_TOKO'] . ' Username: ' . $request['username'] . ' Successfully Log In');
                $request->session()->put('user',$checkUser);
                $prev_destination = $request->session()->get('lastUrl');

                if(isset($prev_destination))
                    $next_destination = $request->session()->get('url.intended');
                else
                    $next_destination = '/';

                return redirect($next_destination);
            }
            else
            {
                Log::error($this->toko['NAMA_TOKO'] . ' Incorrect Password User');
                $request->session()->flash('danger', 'Username atau Passsword Anda Salah');
                return redirect()->route('login');
            }
        }
        else
        {
            Log::error($this->toko['NAMA_TOKO'] . ' Error on login_process Function');
            Log::info('Cause: Request is Empty. Given: ' .json_encode($all_request));

            $request->session()->flash('danger', 'Username atau Passsword Anda Salah');
            return redirect()->route('login');
        }
        
    }

    public function logout()
    {
        Log::info($this->toko['NAMA_TOKO'] . ' Log Out Process');

        if(Session::has('user'))
        {
            $current_user = Session::get('user');

            $last_login = date('Y-m-d H:i:s', strtotime('now'));
            $current_user->LAST_LOGIN = $last_login;
            $current_user->save();

            Session::forget('user');
            Log::info($this->toko['NAMA_TOKO'] . ' ' . $current_user->USERNAME . ' Successfully Log Out');
        }
        
        return redirect()->route('home');
    }

    public function home()
    {
        $user = Session::get('user');
        
        $time = date('H');
        $greeting = 'MORNING';

        if($time >=  "12" && $time < "17")
            $greeting = 'AFTERNOON';
        else if($time >=  "17" && $time < "19")
            $greeting = 'EVENING';
        else if($time >=  "19")
            $greeting = 'NIGHT';

        $this->toko = Helper::checkSession_store();

        $allowed_menu = json_decode($user->MENU);

        $list_menu = [];
        foreach($this->menu as $all_menu)
        {
            if(in_array($all_menu->MENU_DESCRIPTION, $allowed_menu))
                $list_menu[] = $all_menu;
        }

        Log::info($user['USERNAME'] . ' Initiate Home Page');

        return view('index')->with([
            'menu' => 'index',
            'toko' => $this->toko,
            'user' => $user,
            'greeting' => $greeting,
            'list_menu' => $list_menu
        ]);
    }

    public function dashboard()
    {
        $user = Session::get('user');
        $allowed_menu = json_decode($user->MENU);

        if(!in_array('DASHBOARD', $allowed_menu))
            return redirect('/');

        $list_menu = [];
        foreach($this->menu as $all_menu)
        {
            if(in_array($all_menu->MENU_DESCRIPTION, $allowed_menu))
                $list_menu[] = $all_menu;
        }
        
        $this->toko = Helper::checkSession_store();
        $lastUpdate = date('l, d F Y H:i:s');

        Log::info($user['USERNAME'] . ' Initiate Dashboard Page');

        return view('dashboard')->with([
            'menu' => 'dashboard',
            'toko' => $this->toko,
            'last_update' => $lastUpdate,
            'user' => $user,
            'list_menu' => $list_menu
        ]);
    }

    public function income()
    {
        $user = Session::get('user');
        $allowed_menu = json_decode($user->MENU);

        if(!in_array('PEMASUKAN', $allowed_menu))
            return redirect('/');
        
        $list_menu = [];
        foreach($this->menu as $all_menu)
        {
            if(in_array($all_menu->MENU_DESCRIPTION, $allowed_menu))
                $list_menu[] = $all_menu;
        }

        $this->toko = Helper::checkSession_store();

        Log::info($user['USERNAME'] . ' Initiate Income Page');

        return view('income_outcome')->with([
            'menu' => 'income',
            'tittle' => 'PEMASUKAN',
            'toko' => $this->toko,
            'user' => $user,
            'list_menu' => $list_menu
        ]);
    }

    public function outcome()
    {
        $user = Session::get('user');
        $allowed_menu = json_decode($user->MENU);

        if(!in_array('PENGELUARAN', $allowed_menu))
            return redirect('/');
        
        $list_menu = [];
        foreach($this->menu as $all_menu)
        {
            if(in_array($all_menu->MENU_DESCRIPTION, $allowed_menu))
                $list_menu[] = $all_menu;
        }

        $this->toko = Helper::checkSession_store();

        Log::info($user['USERNAME'] . ' Initiate Outcome Page');

        return view('income_outcome')->with([
            'menu' => 'outcome',
            'tittle' => 'PENGELUARAN',
            'toko' => $this->toko,
            'user' => $user,
            'list_menu' => $list_menu
        ]);
    }

    public function store()
    {
        $user = Session::get('user');

        if($user->CREATED_BY != 'SYSTEM')
            return redirect('/');

        $this->toko = Helper::checkSession_store();

        Log::info($user['USERNAME'] . ' Initiate Store Page');

        return view('store')->with([
            'menu' => 'store',
            'toko' => $this->toko,
            'user' => $user,
            'list_menu' => $this->menu
        ]);
    }

    public function user()
    {
        $user = Session::get('user');
        $allowed_menu = json_decode($user->MENU);

        if(!in_array('USER', $allowed_menu))
            return redirect('/');

        $list_menu = [];
        foreach($this->menu as $all_menu)
        {
            if(in_array($all_menu->MENU_DESCRIPTION, $allowed_menu))
                $list_menu[] = $all_menu;
        }

        $this->toko = Helper::checkSession_store();

        Log::info($user['USERNAME'] . ' Initiate User Page');

        return view('user')->with([
            'menu' => 'user',
            'toko' => $this->toko,
            'user' => $user,
            'list_menu' => $list_menu
        ]);
    }

    public function product()
    {
        $user = Session::get('user');
        $allowed_menu = json_decode($user->MENU);

        if(!in_array('PRODUK', $allowed_menu))
            return redirect('/');

        $list_menu = [];
        foreach($this->menu as $all_menu)
        {
            if(in_array($all_menu->MENU_DESCRIPTION, $allowed_menu))
                $list_menu[] = $all_menu;
        }

        $this->toko = Helper::checkSession_store();

        Log::info($user['USERNAME'] . ' Initiate Product Page');

        return view('product')->with([
            'menu' => 'product',
            'toko' => $this->toko,
            'user' => $user,
            'list_menu' => $list_menu
        ]);
    }

    public function archive()
    {
        $user = Session::get('user');
        $allowed_menu = json_decode($user->MENU);

        if(!in_array('ARSIP', $allowed_menu))
            return redirect('/');

        $list_menu = [];
        foreach($this->menu as $all_menu)
        {
            if(in_array($all_menu->MENU_DESCRIPTION, $allowed_menu))
                $list_menu[] = $all_menu;
        }

        $this->toko = Helper::checkSession_store();

        Log::info($user['USERNAME'] . ' Initiate Archive Page');

        return view('archive')->with([
            'menu' => 'archive',
            'toko' => $this->toko,
            'user' => $user,
            'list_menu' => $list_menu
        ]);
    }

    public function employee()
    {
        $user = Session::get('user');
        $allowed_menu = json_decode($user->MENU);

        if($user->CREATED_BY != 'SYSTEM')
            return redirect('/');

        $this->toko = Helper::checkSession_store();

        Log::info($user['USERNAME'] . ' Initiate Employee Page');

        return view('employee')->with([
            'menu' => 'employee',
            'toko' => $this->toko,
            'user' => $user,
            'list_menu' => $this->menu
        ]);
    }

    public function sales()
    {
        $user = Session::get('user');
        $allowed_menu = json_decode($user->MENU);

        if($user->CREATED_BY != 'SYSTEM')
            return redirect('/');

        $this->toko = Helper::checkSession_store();

        Log::info($user['USERNAME'] . ' Initiate Sales Page');

        return view('sales')->with([
            'menu' => 'sales',
            'toko' => $this->toko,
            'user' => $user,
            'list_menu' => $this->menu
        ]);
    }
}
