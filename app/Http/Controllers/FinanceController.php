<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Model\Finance as Finance;
use App\Helpers\Helper as Helper;

use Session;
use DateTime;

class FinanceController extends Controller
{
    public static function calculate_cashflow($profit, $turnover, $data)
    {
        $outcome = 0;
        $income = 0;
        $omset = 0;

        if($data->STATUS == 'PENGELUARAN')
        {
            $outcome = $data->JUMLAH;
            $profit -= $outcome;
        }
        else if($data->STATUS == 'PEMASUKAN')
        {
            $income = $data->JUMLAH;
            $profit += $income;
        }
        else if($data->STATUS == 'OMSET')
        {
            $omset = $data->JUMLAH;
            $turnover += $omset;
        }

        $response = [
            'turnover' => $turnover,
            'income' => $income,
            'outcome' => $outcome,
            'profit' => $profit
        ];
        return $response;
    }
    
    public function load_dashboard()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function load_dashboard');

        $finance=[];
        $today = date('Y-m-d');
        if(Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $finance = Finance::where('ID_TOKO',$getToko->ID_TOKO)->get();
        }

        $this_year_turnover = 0;
        $this_year_income = 0;
        $this_year_outcome = 0;
        $this_year_profit = 0;

        $this_month_turnover = 0;
        $this_month_income = 0;
        $this_month_outcome = 0;
        $this_month_profit = 0;

        $today_turnover = 0;
        $today_income = 0;
        $today_outcome = 0;
        $today_profit = 0;

        foreach($finance as $calculate)
        {
            $data_date = $calculate->TANGGAL;
            
            //this year summary
            if(date('Y', strtotime($data_date)) == date('Y'))
            {
                $this_year = FinanceController::calculate_cashflow($this_year_profit, $this_year_turnover,$calculate);
                $this_year_income += $this_year['income'];
                $this_year_outcome += $this_year['outcome'];
                $this_year_profit = $this_year['profit'];
                $this_year_turnover = $this_year['turnover'];
            }

            // this month summary
            if(date('m-Y', strtotime($data_date)) == date('m-Y'))
            {
                $this_month = FinanceController::calculate_cashflow($this_month_profit, $this_month_turnover, $calculate);
                $this_month_income += $this_month['income'];
                $this_month_outcome += $this_month['outcome'];
                $this_month_profit = $this_month['profit'];
                $this_month_turnover = $this_month['turnover'];
            }

            // today summary
            if(date('Y-m-d', strtotime($data_date)) == $today)
            {
                $this_day = FinanceController::calculate_cashflow($today_profit, $today_turnover, $calculate);
                $today_income += $this_day['income'];
                $today_outcome += $this_day['outcome'];
                $today_profit = $this_day['profit'];
                $today_turnover = $this_day['turnover'];
            }
        }

        $return = [
            'this_year' => [
                'turnover' => Helper::monetize($this_year_turnover),
                'income' => Helper::monetize($this_year_income),
                'outcome' => Helper::monetize($this_year_outcome),
                'profit' => Helper::monetize($this_year_profit)
            ],
            'this_month' => [
                'turnover' => Helper::monetize($this_month_turnover),
                'income' => Helper::monetize($this_month_income),
                'outcome' => Helper::monetize($this_month_outcome),
                'profit' => Helper::monetize($this_month_profit)
            ],
            'today' => [
                'turnover' => Helper::monetize($today_turnover),
                'income' => Helper::monetize($today_income),
                'outcome' => Helper::monetize($today_outcome),
                'profit' => Helper::monetize($today_profit)
            ]
            
        ];
        
        Log::info(Session::get('user')['USERNAME'] . ' Finish Load Dashboard.');
        
        return $return;
    }

    public function get_all_income()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_all_income');

        $all_income = [];
        if(Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $all_income = Finance::where('ID_TOKO',$getToko->ID_TOKO)->where('STATUS','PEMASUKAN')->get();
        }

        $income = [];
        foreach($all_income as $data)
        {
            $income[] = [
                'tanggal' => date('d-m-Y', strtotime($data->TANGGAL)),
                'keterangan' => $data->KETERANGAN,
                'jumlah' => Helper::monetize($data->JUMLAH),
                'status' => $data->STATUS,
                'id' => $data->ID_FINANCE
            ];
        }
        array_multisort($income, SORT_DESC);

        $list = ["data" => $income];

        return $list;
    }

    public function get_all_outcome()
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function get_all_outcome');
        $all_outcome = [];
        if(Session::has('toko'))
        {
            $getToko = Session::get('toko');
            $all_outcome = Finance::where('ID_TOKO',$getToko->ID_TOKO)->where('STATUS','PENGELUARAN')->get();
        }

        $outcome = [];
        foreach($all_outcome as $data)
        {
            $outcome[] = [
                'tanggal' => date('d-m-Y', strtotime($data->TANGGAL)),
                'keterangan' => $data->KETERANGAN,
                'jumlah' => Helper::monetize($data->JUMLAH),
                'status' => $data->STATUS,
                'id' => $data->ID_FINANCE
            ];
        }
        array_multisort($outcome, SORT_DESC);

        $list = ["data" => $outcome];

        return $list;
    }

    public function data_insert($request, $action)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function data_insert For ' . $action);
        $list_finance = [];

        foreach($request as $key=>$data)
        {
            if(!$data)
                return false;
            else if($key == 'tanggal')
                $data = (new DateTime($request['tanggal']))->format('Y-m-d H:i:s');
            
            $list_finance[strtoupper($key)] = strtoupper($data);
        }
        
        if($list_finance && Session::has('toko'))
        {
            $list_finance['STATUS'] = strtoupper($action);
            $getToko = Session::get('toko');
            $list_finance['ID_TOKO'] = $getToko->ID_TOKO;
        }
        return $list_finance;
    }

    public function save_income(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function save_income');
        $data_request = $request->all();
        $status = new \stdClass();

        if($data_request)
        {
            $list_finance = FinanceController::data_insert($data_request, 'pemasukan');

            if($list_finance)
            {
                $insert = Finance::create($list_finance);
                $status->code = 200;
                $status->message = 'Berhasil Tambahkan Pemasukan Baru';
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Income');
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error Insert Income');
                Log::info('Cause: Some Request are Empty (237). Given: ' .json_encode($list_finance));

                $status->code = 500;
                $status->message = 'Terjadi Kesalahan saat Menambahkan Pemasukan';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error Insert Income');
            Log::info('Cause: Some Request are Empty (246). Given: ' .json_encode($data_request));
            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menambahkan Pemasukan';
        }
        return json_encode($status);

    }

    public function save_outcome(Request $request)
    {
        Log::info(Session::get('user')['USERNAME'] . ' Call Function save_outcome');

        $data_request = $request->all();
        $status = new \stdClass();

        if($data_request)
        {
            $list_finance = FinanceController::data_insert($data_request, 'pengeluaran');

            if($list_finance)
            {
                $insert = Finance::create($list_finance);
                $status->code = 200;
                $status->message = 'Berhasil Tambahkan Pengeluaran Baru';
                Log::info(Session::get('user')['USERNAME'] . ' Successfully Insert Outcome');
            }
            else
            {
                Log::error(Session::get('user')['USERNAME'] . ' Error Insert Outcome');
                Log::info('Cause: Some Request are Empty (275). Given: ' .json_encode($list_finance));

                $status->code = 500;
                $status->message = 'Terjadi Kesalahan saat Menambahkan Pengeluaran';
            }
        }
        else
        {
            Log::error(Session::get('user')['USERNAME'] . ' Error Insert Outcome');
            Log::info('Cause: Some Request are Empty (284). Given: ' .json_encode($data_request));

            $status->code = 500;
            $status->message = 'Terjadi Kesalahan saat Menambahkan Pengeluaran';
        }
        
        return json_encode($status);

    }
    
}

?>