<?php

namespace App\Http\Middleware;

use Closure;
// use Illuminate\Support\Facades\Auth;

class AuthCustom
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = $request->session()->get('user');
        if (empty($user)) {
            $url = $request->path();
            session()->put('url.intended', $request->fullUrl());
            session()->put('lastUrl', $url);
            return redirect()->route('login');
        }

        return $next($request);
    }
}
