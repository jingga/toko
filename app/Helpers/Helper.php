<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\Model\Toko as Toko;
use App\Model\Menu as Menu;

use Session;

class Helper
{
    public static function monetize($amount)
    {
        return "Rp ". number_format(floatval($amount),0,",",".");
    }

    public static function checkSession_store()
    {
        $current_host = $_SERVER['HTTP_HOST'];
        $current_toko = Toko::where('URL_TOKO', $current_host)->first();

        $current_toko;
        Session::put('toko',$current_toko);
        return $current_toko;
    }

    public static function get_menu()
    {
        $all_menu = Menu::where('STATUS','ACTIVE')->get();
        return $all_menu;
    }

    public static function generate_invoice_number($toko, $rowNum)
    {
        $date = date('Ymd');
        $invoice_number = strtoupper($toko).$date.str_pad($rowNum,3,'0',STR_PAD_LEFT);
        return $invoice_number;
    }

    //crop image to thumbnail
    public static function crop_img ($image, $thumb_width, $thumb_height) {
        $filename = $image;
        $image = imagecreatefromstring(file_get_contents($image));
     
        $width = imagesx($image);
        $height = imagesy($image);
     
        $original_aspect = $width / $height;
        $thumb_aspect = $thumb_width / $thumb_height;
     
        if ( $original_aspect >= $thumb_aspect )
        {
           // If image is wider than thumbnail (in aspect ratio sense)
           $new_height = $thumb_height;
           $new_width = $width / ($height / $thumb_height);
        }
        else
        {
           // If the thumbnail is wider than the image
           $new_width = $thumb_width;
           $new_height = $height / ($width / $thumb_width);
        }
     
        $thumb = imagecreatetruecolor($thumb_width, $thumb_height);
     
        // Resize and crop
        imagecopyresampled($thumb,
            $image,
            0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
            0 - ($new_height - $thumb_height) / 2, // Center the image vertically
            0, 0,
            $new_width, $new_height,
            $width, $height);
        
        return imagejpeg($thumb, $filename, 80);
    }
}

?>