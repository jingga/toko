<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Toko extends Model
{
    use \App\Traits\Uuids;

    protected $table = 'toko';
    public $incrementing = false;
    protected $primaryKey = 'ID_TOKO';
    
    protected $fillable = 
    [
        'NAMA_TOKO',
        'URL_TOKO',
        'LOGO_TOKO',
        'STATUS',
        'CREATED_BY',
        'UPDATED_BY',
        'CREATED_AT',
        'UPDATED_AT',
    ];
}
