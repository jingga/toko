<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Absence_History extends Model
{
    use \App\Traits\Uuids;

    protected $table = 'absence_history';
    public $incrementing = false;
    protected $primaryKey = 'ID_ABSENCE';

    protected $fillable = 
    [
        'ID_TOKO',
        'ID_KARYAWAN',
        'TANGGAL',
        'STATUS',
        'CREATED_AT',
        'UPDATED_AT'
    ];
}
