<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use \App\Traits\Uuids;

    protected $table = 'user';
    public $incrementing = false;
    protected $primaryKey = 'ID_USER';

    protected $fillable = 
    [
        'ID_TOKO',
        'USERNAME',
        'PASSWORD',
        'ROLE',
        'STATUS',
        'MENU',
        'CREATED_BY',
        'UPDATED_BY',
        'LAST_LOGIN',
        'CREATED_AT',
        'UPDATED_AT'
    ];
}
