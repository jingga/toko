<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use \App\Traits\Uuids;

    protected $table = 'product';
    public $incrementing = false;
    protected $primaryKey = 'ID_PRODUCT';
    
    protected $fillable = 
    [
        'ID_TOKO',
        'NAMA_PRODUCT',
        'STOCK_PRODUCT',
        'MODAL_PRODUCT',
        'HARGA_PRODUCT',
        'LOKASI_PRODUCT',
        'STATUS',
        'CREATED_AT',
        'UPDATED_AT'
    ];
}
