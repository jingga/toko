<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Stock_History extends Model
{
    use \App\Traits\Uuids;
    
    protected $table = 'stock_history';
    public $incrementing = false;
    protected $primaryKey = 'ID_STOCK';

    protected $fillable = 
    [
        'ID_TOKO',
        'ID_PRODUCT',
        'SEQUENCE',
        'TANGGAL',
        'DESCRIPTION',
        'QTY',
        'STOCK_BEFORE',
        'STOCK_AFTER',
        'CREATED_AT',
        'UPDATED_AT'
    ];
}
