<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    use \App\Traits\Uuids;

    protected $table ='archive';
    public $incrementing = false;
    protected $primaryKey = 'ID_ARCHIVE';
    
    protected $fillable = 
    [
        'ID_TOKO',
        'TANGGAL',
        'NAMA_KONSUMEN',
        'ALAMAT_KONSUMEN',
        'PONSEL_KONSUMEN',
        'DETAIL_PRODUCT',
        'DISCOUNT',
        'SELLER',
        'STATUS',
        'CREATED_AT',
        'UPDATED_AT'
    ];
}
