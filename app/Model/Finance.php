<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    use \App\Traits\Uuids;

    protected $table = 'finance';
    public $incrementing = false;
    protected $primaryKey = 'ID_FINANCE';
    
    protected $fillable = 
    [
        'ID_TOKO',
        'TANGGAL',
        'KETERANGAN',
        'JUMLAH',
        'STATUS',
        'CREATED_AT',
        'UPDATED_AT'
    ];
}
