<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    use \App\Traits\Uuids;

    protected $table = 'karyawan';
    public $incrementing = false;
    protected $primaryKey = 'ID_KARYAWAN';

    protected $fillable = 
    [
        'ID_TOKO',
        'NAMA_KARYAWAN',
        'STATUS',
        'SALARY',
        'SALARY_TYPE',
        'SALARY_DUE',
        'BONUS_MONTHLY',
        'BONUS',
        'CREATED_AT',
        'UPDATED_AT'
    ];
}
