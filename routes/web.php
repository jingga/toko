<?php

Route::get('/', 'HomeController@home')->middleware('authcustom')->name('home');
Route::get('/login', 'HomeController@login_page')->name('login');
Route::get('/dashboard', 'HomeController@dashboard')->middleware('authcustom')->name('dashboard');
Route::get('/income', 'HomeController@income')->middleware('authcustom')->name('income');
Route::get('/outcome', 'HomeController@outcome')->middleware('authcustom')->name('outcome');
Route::get('/store', 'HomeController@store')->middleware('authcustom')->name('store');
Route::get('/user', 'HomeController@user')->middleware('authcustom')->name('user');
Route::get('/product', 'HomeController@product')->middleware('authcustom')->name('product');
Route::get('/invoice', 'HomeController@invoice')->middleware('authcustom')->name('invoice');
Route::get('/archive', 'HomeController@archive')->middleware('authcustom')->name('archive');
Route::get('/employee', 'HomeController@employee')->middleware('authcustom')->name('employee');
Route::get('/sales', 'HomeController@sales')->middleware('authcustom')->name('sales');

/* LIST API */

/* LOGIN-LOGOUT PROCESS */
Route::post('/process_login', 'HomeController@login_process')->name('processLogin');
Route::get('/logout', 'HomeController@logout')->name('logout');

/* PRODUCT */
Route::post('/get_all_product', 'ProductController@get_all_product')->name('getAllProduct');
Route::get('/load_all_product', 'ProductController@load_all_product')->name('getProductList');
Route::post('/insert_product', 'ProductController@insert_product_to_db')->name('insertProduct');
Route::post('/update_product', 'ProductController@update_product_to_db')->name('updateProduct');
Route::post('/delete_product', 'ProductController@delete_product_from_db')->name('deleteProduct');
Route::post('/get_history_product', 'ProductController@get_list_history_of_product')->name('getHistoryProduct');
Route::post('/update_stock_product', 'ProductController@update_stock_product_history')->name('updateStockProduct');

/* NOTA/ARCHIVE */
Route::post('/save_invoice', 'ArchiveController@save_invoice')->name('save_invoice');
Route::get('/preview_invoice/{id?}', 'ArchiveController@preview_invoice')->name('print_invoice');
Route::post('/get_all_archive', 'ArchiveController@get_all_archive')->name('getAllArchive');
Route::post('/update_archive', 'ArchiveController@update_status_archive_to_lunas')->name('updateStatusArchive');

/* FINANCE */
Route::post('/load_dashboard', 'FinanceController@load_dashboard')->name('loadDashboard');
Route::post('/get_all_income', 'FinanceController@get_all_income')->name('getIncomeList');
Route::post('/get_all_outcome', 'FinanceController@get_all_outcome')->name('getOutcomeList');
Route::post('/save_income', 'FinanceController@save_income')->name('save_income');
Route::post('/save_outcome', 'FinanceController@save_outcome')->name('save_outcome');

/* User */
Route::post('/get_all_user', 'UserController@get_all_user')->name('getAllUser');
Route::post('/insert_user', 'UserController@insert_user_to_db')->name('insertUser');
Route::post('/update_user', 'UserController@update_user_to_db')->name('updateUser');
Route::post('/delete_user', 'UserController@delete_user_from_db')->name('deleteUser');

/* WebService Register User */
Route::post('/register_user', 'UserController@service_register_user')->name('registerUser');

/* EMPLOYEE */
Route::post('/get_all_employee', 'EmployeeController@get_all_employee')->name('getAllEmployee');
Route::post('/get_employee_absence', 'EmployeeController@get_employee_absence')->name('getEmployeeAbsence');
Route::post('/insert_employee', 'EmployeeController@insert_employee')->name('insertEmployee');
Route::post('/update_employee', 'EmployeeController@update_employee')->name('updateEmployee');
Route::post('/delete_employee', 'EmployeeController@delete_employee')->name('deleteEmployee');
Route::post('/update_absence', 'EmployeeController@update_absence')->name('updateAbsence');

/* STORE */
Route::post('/get_all_store', 'StoreController@get_all_store')->name('getAllStore');
Route::post('/insert_store', 'StoreController@insert_store')->name('insertStore');
Route::post('/update_store', 'StoreController@update_store')->name('updateStore');
Route::post('/update_store', 'StoreController@update_store')->name('updateStore');
Route::post('/delete_store', 'StoreController@delete_store')->name('deleteStore');